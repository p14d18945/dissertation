function [nD] = getDirichletNodes(nelx, nely)
% leftnodes of mesh
for i = 1 : nely + 1
    k = (i - 1);
    dv(i) = k * 2 *(nelx + 1) + 1;
    dh(i) = k * 2 *(nelx + 1) + 2;
end
nD = [dv', dh'];
end

