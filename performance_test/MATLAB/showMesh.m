function [cord] = showMesh(nelx, nely, a, b, uhat ,Factor, ElemID)

if nargin<5
    showDisplacement =0;
else
    showDisplacement = 1;
end
    

xcord = linspace(0,nelx*a,nelx+1);
ycord = linspace(0,nely*b,nely+1);
cnt = 0;
for j = 1:length(ycord)
    for i = 1:length(xcord)
        cnt = cnt+1;
        cord(2*cnt-1) = xcord(i);
        cord(2*cnt) = ycord(j);
    end
end

nE = (nelx)*(nely);

hold on
    for j = 1:nE   
        %startposition
        x1 = ElemID(j,1);
        y1 = ElemID(j,2);
        x2 = ElemID(j,3);
        y2 = ElemID(j,4);
        x3 = ElemID(j,5);
        y3 = ElemID(j,6);
        x4 = ElemID(j,7);
        y4 = ElemID(j,8);
        
   
        plot([cord(x1),cord(x2)],[cord(y1), cord(y2)],'.k','MarkerSize',10)
        plot([cord(x2),cord(x3)],[cord(y2), cord(y3)],'.k','MarkerSize',10)
        plot([cord(x3),cord(x4)],[cord(y3), cord(y4)],'.k','MarkerSize',10)
        plot([cord(x4),cord(x1)],[cord(y4), cord(y1)],'.k','MarkerSize',10)
        plot([cord(x1),cord(x2)],[cord(y1), cord(y2)],'k')
        plot([cord(x2),cord(x3)],[cord(y2), cord(y3)],'k')
        plot([cord(x3),cord(x4)],[cord(y3), cord(y4)],'k')
        plot([cord(x4),cord(x1)],[cord(y4), cord(y1)],'k')
        
        if showDisplacement
        uFac = Factor*uhat;
        %displacedPosition
        plot([cord(x1)+uFac(x1),cord(x2)+uFac(x2)],[cord(y1)+uFac(y1), cord(y2)+uFac(y2)],'.r','MarkerSize',10)
        plot([cord(x2)+uFac(x2),cord(x3)+uFac(x3)],[cord(y2)+uFac(y2), cord(y3)+uFac(y3)],'.r','MarkerSize',10)
        plot([cord(x3)+uFac(x3),cord(x4)+uFac(x4)],[cord(y3)+uFac(y3), cord(y4)+uFac(y4)],'.r','MarkerSize',10)
        plot([cord(x4)+uFac(x4),cord(x1)+uFac(x1)],[cord(y4)+uFac(y4), cord(y1)+uFac(y1)],'.r','MarkerSize',10)
        plot([cord(x1)+uFac(x1),cord(x2)+uFac(x2)],[cord(y1)+uFac(y1), cord(y2)+uFac(y2)],'r')
        plot([cord(x2)+uFac(x2),cord(x3)+uFac(x3)],[cord(y2)+uFac(y2), cord(y3)+uFac(y3)],'r')
        plot([cord(x3)+uFac(x3),cord(x4)+uFac(x4)],[cord(y3)+uFac(y3), cord(y4)+uFac(y4)],'r')
        plot([cord(x4)+uFac(x4),cord(x1)+uFac(x1)],[cord(y4)+uFac(y4), cord(y1)+uFac(y1)],'r')
        end
    end



hold off
%xlim([-a ((nelx)*a)+a])
%ylim([-b ((nely)*b)+b])
axis equal
end

