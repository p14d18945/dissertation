function [t] = getTMatrix(nelx, nely)
%GETTMATRIX Summary of this function goes here
%   Detailed explanation goes here
nE = nelx * nely;
t = zeros(nE,8);
for e = 1:nE
    t(e,:) = getElementTVector(e, nelx, nely);
end
end

function Te = getElementTVector(e, nelx, nely)
nx = nelx;
i = e-1;
row  = i/nx - mod((i/nx), 1);
n = zeros(4,1);
n(1:2) = row + i + [1,2];
n(3:4) = row + i + [nx+3,nx+2];


% compute T-Vector
nn =  length(n);
Te = zeros(1, 2 * nn);
% first sequence
subtr = 0:3;
s1 = (1:4) + subtr.*2-subtr;

% second sequence
s2 = (1:4)*2;
subtr = 2-1;
Te(s1) = n*2-subtr;
Te(s2) = n*2;
end
