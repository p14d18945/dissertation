function [k] = assembleK(nelx, nely, fac ,nD, tMat, loop)
%ASSEMBLEK Summary of this function goes here
%   Detailed explanation goes here
%% initialize datas

% ke :  elementStiffnessMatrix
ke = stiffnessMatrix(fac);

nN = (nelx + 1) * (nely + 1) * 2;
nE = nelx * nely;
k = zeros(nN, nN);
% Assemble K column and rowwise (way faster than elementwise)
t = tMat;

if ~loop
    % duplicate 8-times number of nodes per element
    tx = repmat(t',8,1); %safe
    % build column vector
    colT = tx(:);
    % transpose
    tt = t'; %safe
    % duplicate 8-times number of nodes per element
    tx = repmat(tt(:),1,8).'; %safe
    % build row vector
    rowT = tx(:); %safe
    
    % duplicate number of elements-time
    knE = repmat(ke',1,nE);
    % build vector;
    keVec = knE(:);
    
    % build sparsed matrix
    k = sparse(rowT ,colT,keVec,nN,nN);
    
else
    
    for e = 1 : nE
        n = length(ke);
        Te = tMat(e,:);
        for i = 1:n
            for j = 1:n
                I = Te(i);
                J = Te(j);
                k(I,J) = k(I,J) + ke(i,j);
            end
        end
    end
    
    
   % k = sparse(k);
end
% pick up true DOFs

d = reshape(nD',length(nD(:)),1);

% check if any nodes are fixed
if isempty(d)
    error('No fixed nodes, check Dirichlet-BCs!')
end
% fix DOFs

for i = 1:length(d)
    j = d(i);
    k(j,:) = 0; %#ok<SPRIX>
    k(:,j) = 0; %#ok<SPRIX>
    k(j,j) = 1; %#ok<SPRIX>
end

end

function ke = stiffnessMatrix(fac)
% Calculate Stiffnessmatrix for Quad4-Elements
% Emod: young's modulus
% h: thickness
% a: elementhigh
% b: elementwidth
Emod = 210000*10^4;
%Emod = 2.1e11
v = 0.3;
a = 0.01*fac;
b = 0.01*fac;
h = 0.2;


kloc = [4*a^2+2*b^2*(1-v)   ,  3*a*b*(1+v)/2      , -4*a^2+b^2*(1-v)   , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)   , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v)  ,  3*a*b*(1-3*v)/2   ;
    3*a*b*(1+v)/2       ,  4*b^2+2*a^2*(1-v)  ,  3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2    , -4*b^2+a^2*(1-v)   ;
    -4*a^2+b^2*(1-v)     ,  3*a*b*(1-3*v)/2    ,  4*a^2+2*b^2*(1-v) , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v) , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)    ,  3*a*b*(1+v)/2     ;
    -3*a*b*(1-3*v)/2     ,  2*b^2-2*a^2*(1-v)  , -3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2      , -2*b^2-a^2*(1-v)   ;
    -2*a^2-b^2*(1-v)     , -3*a*b*(1+v)/2      ,  2*a^2-2*b^2*(1-v) ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v) ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)    , -3*a*b*(1-3*v)/2   ;
    -3*a*b*(1+v)/2       , -2*b^2-a^2*(1-v)    , -3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2    ,  2*b^2-2*a^2*(1-v) ;
    2*a^2-2*b^2*(1-v)   , -3*a*b*(1-3*v)/2    , -2*a^2-b^2*(1-v)   ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)   ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v)  , -3*a*b*(1+v)/2     ;
    3*a*b*(1-3*v)/2     , -4*b^2+a^2*(1-v)    ,  3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2      ,  4*b^2+2*a^2*(1-v)];


ke = kloc *(Emod*h/(12*a*b*(1-v^2)));
end


