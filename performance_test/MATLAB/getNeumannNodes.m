function [nN] = getNeumannNodes(nelx, nely)
% leftnodes of mesh
for i = 1 : nely + 1
    nv(i) = i * 2 *(nelx + 1) - 1;
    nh(i) = i * 2 *(nelx + 1);
end
nN = [nv', nh'];
end

