classdef CircularVoid < topopt.fefunc.Void
    % SUPERCLASS Void
    % usable to construct circular holes
    
    properties (Hidden)
        % c: Centerpoint
        % r: Radius
        c
        r
    end
        
    methods
        function this = CircularVoid(c, r)
            % set properties
            this.c  = c;
            this.r = r;
        end
        
        function b = isInside(this, p)
            % subclass based abstract superclass Method
            % check if point is in the void-zone
            
            % output b: boolean
            % input p: testpoint
            
            x = p(1);
            y = p(2);
            
            % check if current node position is inside the radius of the 
            % circular hole
            if sqrt((y-this.c(2))^2+(x-this.c(1))^2) < this.r
                b =  1;
            else
                b = 0;
            end
                       
        end
        
    end
end

    