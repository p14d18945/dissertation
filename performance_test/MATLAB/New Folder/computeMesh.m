function [Mesh] = computeMesh(width, high, eSize)
%COMPUTEMESH Summary of this function goes here
%   Detailed explanation goes here
Mesh.width = width;
Mesh.high = high;

restx = mod(1/eSize*width,1);
Mesh.sizex  = (restx*eSize)/(1/eSize*width-restx)+eSize;
Mesh.nelx = width/Mesh.sizex;
Mesh.vecX = linspace(0, width, Mesh.nelx+1);

resty = mod(1/eSize*high,1);
Mesh.sizey  = (resty*eSize)/(1/eSize*high-resty)+eSize;
Mesh.nely = high/Mesh.sizex;
Mesh.vecY = linspace(0,high, Mesh.nely+1);

[Mesh.XCord,Mesh.YCord] = meshgrid(Mesh.vecX, Mesh.vecY);
end


