classdef Void < handle
    
    methods 
        function this = Void()
        end

        function ids = getElementIDs(this, mesh)

            nE = mesh.countElements();
          
            ids = zeros(nE,1);
            for i = 1:nE
                p = mesh.getElementCenter(i);
                ids(i)  =  this.isInside(p);
            end    
        end        
        
    end
    
    
    methods (Abstract)
        b = isInside(p);
    end
end

