classdef RectangularVoid < topopt.fefunc.Void
    % SUPERCLASS Void
    % Usable to construct rectangular holes
    
    properties (Hidden)
        % p1(x,y): left topcorner point
        % p2(x,y): right buttomcorner point
        p1
        p2
    end
    
    methods
        function this = RectangularVoid(p1, p2)
            this.p1 = p1;
            this.p2 = p2;
        end
        
        
        function b = isInside(this, p)
            % subclass based abstract superclass Method
            % check if point is in the void-zone
            
            % output b: boolean
            % input p: testpoint
          
            x = p(1);
            y = p(2);
            if x < this.p2(1) && x > this.p1(1) && y < this.p2(2) && y > this.p1(2)
                b =  1;
            else
                b = 0;
            end
        end   
    
    end
    
end

