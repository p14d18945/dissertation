function [x,uHat] = pile_fem(Ne,p)

E = p.E;
D = p.d;
L = p.l;
F = p.F;
rho = p.rho;
g = p.g;
C = p.C;
S = p.S;
h = L/(Ne);
A = pi * D^2/4;
N = Ne + 1;
n = rho*A*g;

EA = E*A;
kEA = zeros(N);
kc = zeros(N);
ks = zeros(N);
rN = zeros(0,N);
rF = zeros(0,N);
x = zeros(0,N);

for i = 1 : N
    x(i) = (i-1)*h;
    if (i == 1 || i == N)
        rN(i) = 1;
        if (i < 2)
            rF(i) = F;
        else
            rF(i) = 0;
        end
    else
        rF(i) = 0;
        rN(i) = 2;
    end
    
    for j = 1 : N
        if (i == 1 && j == 1)
            kEA(i,j) = 1;
            kc(i,j) = 2;
        elseif(i == 1 && j == 2)
            kEA(i,j) = -1;
            kc(i,j) = 1;
        elseif(i > 1 && i < N)
            if (i == j)
                kEA(i,j) = 2;
                kc(i,j) = 4;
            end
            
            if (i == j + 1 | i == j-1)
                kEA(i,j) = -1;
                kc(i,j) = 1;
            end
        elseif(i == N && j == N-1)
            kEA(i,j) = -1;
            kc(i,j) =1;
        elseif(i == N && j == N)
            kEA(i,j) =1;
            kc(i,j) =2;
            ks(i,j) = 1;
        end
    end
    
end

K = (EA/h)*kEA+(C*h/6)*kc+S*ks;
r = ((n*h/2)*rN+rF).';
uHat = linsolve(K,r);

end

