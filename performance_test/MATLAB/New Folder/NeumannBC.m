classdef NeumannBC < topopt.fefunc.BoundaryCondition
    %NEUMANN BOUNDARY CONDITION
    properties (Hidden)
        
        q % vector of values
    end
    methods (Hidden)
        
        function  r = getR(this, values, mesh)
            nN = mesh.countNodes();
            r = zeros(2*nN,1);
            for i = 1:nN
                % IDs for vertical and horizontal DOFs
                % example:
                %          dir = [0, 1], node=2
                %          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
                id = [(i*2-1), i*2];
                
                % get specific node position
                p = mesh.getNodePosition(i);
                if this.isOnBoundary(p)
                    % check if node is on boundary, set dof = true/ false;
                    r(id(1)) = r(id(1)) + values(1);
                    r(id(2)) = r(id(2)) + values(2);
                end
                %r(id) = r(id) + value;
            end
            
        end
        
    end
    
    methods
        function setValues(this, values)
            % set values
            this.q = values;
        end
        
        function values = getValues(this)
            % get values
            values = this.q;
        end
    end
end

