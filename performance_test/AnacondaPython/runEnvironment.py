 # Set "nopython" mode for best performance, equivalent to @njit
import timeit
code_to_test = """
import numpy as np
import numpy.matlib
from scipy import sparse
import scipy.sparse.linalg as spl
#from scipy.sparse.linalg import spsolve
from getTMatrix import TMatrix
from getBCNodes import getDirichletNodes
from getBCNodes import getNeumannNodes
from assembleK import assembleK
import numba
from numba import jit
n = 10
nelx = n
nely = n
fac = 10
nN = (nelx + 1) * (nely + 1) * 2
np.set_printoptions(edgeitems=nN)
nD = getDirichletNodes(nelx, nely)
tMat = TMatrix(nelx, nely)
k = assembleK(nelx,nely,1,nD,tMat,0)
r = np.zeros((nN,1))
nID = getNeumannNodes(nelx, nely)
f = 10
fi = f/(nID.size/2)
for i in range(0, int((nID.size/2))):
    iD = int(nID[(i,1)])
    r[iD-1] = -fi

#uHat = np.linalg.solve(k, r)
I=np.setdiff1d(np.arange(k.shape[0]),nD-1)
uHat=spl.spsolve(k[I].T[I].T,r[I])
"""
elapsed_time = timeit.timeit(code_to_test, number=100)/100
print(elapsed_time)
#print(uHat[200])


#np.savetxt('text.txt',k,fmt='%.6f')

#print(uHat)
