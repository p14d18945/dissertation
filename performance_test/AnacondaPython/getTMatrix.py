import numpy as np

def TVector(e, nelx, nely):
    nx = nelx
    i = e - 1
    row = i / nx - ((i / nx) % 1)
    n = np.zeros(4)
    n[[0, 1]] = [1, 2]
    n[[2, 3]] = [nx + 3, nx + 2]
    n = np.array([x + row + i for x in n])
    # compute T - Vector
    nn = n.size
    Te = np.zeros(2 * nn)
    # first sequence
    subtr = np.arange(0, 4)
    s1 = np.arange(0, 8, 2)

    # second sequence
    s2 = np.arange(1, 9, 2);

    subtr = 2 - 1;
    Te[s1] = [n * 2 - subtr]
    Te[s2] = [n * 2];
    return Te

def TMatrix(nelx, nely):
    nE = nelx * nely
    t = np.zeros((nE, 8))
    for i in range(0, nE):
        t[i] = TVector(i+1, nelx, nely)
    return t

