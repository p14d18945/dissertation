import numpy as np

def getDirichletNodes(nelx, nely):
    nE = nelx * nely
    nD = np.zeros((nely+1, 2))
    for i in range(0, nely + 1):
        nD[i, 0] = int(i * 2 * (nelx + 1) + 1)
        nD[i, 1] = int(i * 2 * (nelx + 1) + 2)
    return nD

def getNeumannNodes(nelx, nely):
    nE = nelx * nely
    nN = np.zeros((nely+1, 2))
    for i in range(0, nely + 1):
        k = i + 1
        nN[i, 0] = int(k * 2 * (nelx + 1) - 1)
        nN[i, 1] = int(k * 2 * (nelx + 1))
    return nN

