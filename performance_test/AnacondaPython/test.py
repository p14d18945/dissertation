import timeit
code_to_test = """
from topopt import main
main(10,10,0.4,3,4,1)
"""
elapsed_time = timeit.timeit(code_to_test, number=1)/1
print(elapsed_time)