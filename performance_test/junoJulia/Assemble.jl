using SparseArrays
function stiffnessMatrix(fac)
    # Calculate Stiffnessmatrix for Quad4-Elements
    # Emod: young's modulus
    # h: thickness
    # a: elementhigh
    # b: elementwidth
    #Emod = 210000*10^4
    Emod = 1
    v = 0.3
    a = 0.01*fac
    b = 0.01*fac
    h = 0.2

    kloc = Array{Float64}(undef, 8, 8)
    kloc[1,:] = [4*a^2+2*b^2*(1-v) , 3*a*b*(1+v)/2 , -4*a^2+b^2*(1-v) , -3*a*b*(1-3*v)/2 , -2*a^2-b^2*(1-v) , -3*a*b*(1+v)/2 , 2*a^2-2*b^2*(1-v) , 3*a*b*(1-3*v)/2]
    kloc[2,:] = [3*a*b*(1+v)/2 , 4*b^2+2*a^2*(1-v) , 3*a*b*(1-3*v)/2 ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2    , -4*b^2+a^2*(1-v)]
    kloc[3,:] = [-4*a^2+b^2*(1-v) , 3*a*b*(1-3*v)/2    ,  4*a^2+2*b^2*(1-v) , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v) , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)    ,  3*a*b*(1+v)/2]
    kloc[4,:] = [-3*a*b*(1-3*v)/2     ,  2*b^2-2*a^2*(1-v)  , -3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2      , -2*b^2-a^2*(1-v)]
    kloc[5,:] = [-2*a^2-b^2*(1-v)     , -3*a*b*(1+v)/2      ,  2*a^2-2*b^2*(1-v) ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v) ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)    , -3*a*b*(1-3*v)/2]
    kloc[6,:] = [-3*a*b*(1+v)/2       , -2*b^2-a^2*(1-v)    , -3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2    ,  2*b^2-2*a^2*(1-v)]
    kloc[7,:] = [2*a^2-2*b^2*(1-v)   , -3*a*b*(1-3*v)/2    , -2*a^2-b^2*(1-v)   ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)   ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v)  , -3*a*b*(1+v)/2]
    kloc[8,:] = [3*a*b*(1-3*v)/2     , -4*b^2+a^2*(1-v)    ,  3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2      ,  4*b^2+2*a^2*(1-v)]


    ke = kloc *(Emod*h/(12*a*b*(1-v^2)))
    return ke
end;

function assembleK(nelx, nely, nD, tMat, loop, volfrac, penal)
    fac = 1;
    ke = stiffnessMatrix(fac)
    nN = (nelx + 1) * (nely + 1) * 2
    nE = nelx * nely
    x  = fill(volfrac, nely,nelx)
    if !(loop)
        t = tMat
        # duplicate 8-times number of nodes per element
        tx = repeat(t',8,1) #safe

        #build column vector
        colT = tx[:]
        #transpose
        tt = t' #safe
        #duplicate 8-times number of nodes per element
        tx = repeat(tt[:],1,8)' #safe
        #build row vector
        rowT = tx[:] #safe

        #duplicate number of elements-time
        knE = repeat(ke',1,nE)
        #build vector;
        keVec = knE[:]

        #build sparsed matrix
        # Pkg.add("stdlib")
        # Pkg.add("Linear Algebra
        dofs = (nelx + 1) * (nely + 1) * 2;
        k = sparse(rowT ,colT, keVec, dofs, dofs)
    else
        k = Array{Float64}(undef, nN, nN)
        xe = x'[:]'
        for e = 1 : nE
            n = 8
            Te = tMat[e,:]'
            for i = 1:n
                for j = 1:n
                    I = Te[i]
                    J = Te[j]
                    k[I,J] += x[e]^penal.+ke[i,j]
                end
            end
        end
    end

    d = [nD[1] ; nD[2]]
    for i = 1:length(d)
        j = d[i]
        k[j,:] .= 0.0
        k[:,j] .= 0.0
        k[j,j] = 1.0
    end
    return k
end;

function assembleR(nelx, nely, nN)
    r = zeros((nelx+1)*(nely+1)*2,1)
    nload = copy(nN[2])
    f = 10
    fi = f/(length(nload))
    r[nload] .= -fi
    return r
end;
