function getDirichletNodes(nelx, nely)
    n = nely + 1
    dv = Array{Int64}(undef,n)
    dh = Array{Int64}(undef, n)
    for i in 1 : nely + 1
        k = (i - 1)
        dv[i] = k * 2 *(nelx + 1) + 1
        dh[i] = k * 2 *(nelx + 1) + 2
    end
    nD = Array{Int32}[dv , dh];
    return nD
end;

function getNeumannNodes(nelx, nely)
    n = nely + 1
    dv = Array{Int64}(undef,n)
    dh = Array{Int64}(undef, n)
    for i in 1 : nely + 1
        dv[i] = i * 2 *(nelx + 1) - 1
        dh[i] = i * 2 *(nelx + 1)
    end
    nD = Array{Int64}[dv , dh];
    return nD
end;





#export x, y

#end
