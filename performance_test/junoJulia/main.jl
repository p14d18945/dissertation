include("Assemble.jl"); include("BCs.jl"); include("TMat.jl")
import SuiteSparse.CHOLMOD.FactorComponent
using SparseArrays; using LinearAlgebra; using Distributions;
#cholfact(A) = cholesky(A, Val(false); check = true)
# Pkg.add("MatrixDepot") for cholfact
# Pkg.add("SuiteSparse")
# Pkg.add("Distributions")
#@time begin
function test(n, ne)
        for i = 1:n
        nelx = ne; nely = ne
        nD = getDirichletNodes(nelx, nely)
        nN = getNeumannNodes(nelx, nely)
        tMat = getTMatrix(nelx, nely)
        k = assembleK(nelx, nely, 10 ,nD, tMat, false)
        r = assembleR(nelx, nely, nN)
        uHat = k\r
    end
end;

@time test(100, 100)

#lu(k)
#uHat = cholesky(k)\r
#uHat = lu(k)\r;
