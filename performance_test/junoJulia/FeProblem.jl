using SparseArrays; using LinearAlgebra;

function getDirichletNodes(nelx, nely)
    n = nely + 1
    dv = Array{Int64}(undef,n)
    dh = Array{Int64}(undef, n)
    for i in 1 : nely + 1
        k = (i - 1)
        dv[i] = k * 2 *(nelx + 1) + 1
        dh[i] = k * 2 *(nelx + 1) + 2
    end
    nD = Array{Int32}[dv , dh];
    return nD
end;

function getNeumannNodes(nelx, nely)
    n = nely + 1
    dv = Array{Int64}(undef,n)
    dh = Array{Int64}(undef, n)
    for i in 1 : nely + 1
        dv[i] = i * 2 *(nelx + 1) - 1
        dh[i] = i * 2 *(nelx + 1)
    end
    nD = Array{Int64}[dv , dh];
    return nD
end;

function getTVector(e, nelx, nely)
    nx = nelx
    i = e-1
    row = i/nx - mod((i/nx), 1)
    n = fill(Int64(row+i), 1,4)
    n[1:2] .+= 1:2
    n[3:4] .+= [nx+3,nx+2]

    # compute T-Vector
    Te = Array{Int64}(undef,8)
    # first sequence
    subtr = 0:3
    s1 = (1:4) + subtr.*2-subtr

    # second sequence
    s2 = (1:4)*2
    subtr = 2-1
    Te[s1] = n*2 .-subtr
    Te[s2] = n*2
    return Te
end;

function getTMatrix(nelx, nely)
    nE = nelx * nely
    t = Array{Int64}(undef,nE, 8) #zeros(nE,8);
    for e = 1:nE
        t[e,:] = getTVector(e, nelx, nely);
    end
    return t
end;

using SparseArrays
function stiffnessMatrix(fac)
    # Calculate Stiffnessmatrix for Quad4-Elements
    # Emod: young's modulus
    # h: thickness
    # a: elementhigh
    # b: elementwidth
    Emod = 210000*10^4
    #Emod = 2.1e11
    v = 0.3
    a = 0.01*fac
    b = 0.01*fac
    h = 0.2

    kloc = Array{Float64}(undef, 8, 8)
    kloc[1,:] = [4*a^2+2*b^2*(1-v) , 3*a*b*(1+v)/2 , -4*a^2+b^2*(1-v) , -3*a*b*(1-3*v)/2 , -2*a^2-b^2*(1-v) , -3*a*b*(1+v)/2 , 2*a^2-2*b^2*(1-v) , 3*a*b*(1-3*v)/2]
    kloc[2,:] = [3*a*b*(1+v)/2 , 4*b^2+2*a^2*(1-v) , 3*a*b*(1-3*v)/2 ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2    , -4*b^2+a^2*(1-v)]
    kloc[3,:] = [-4*a^2+b^2*(1-v) , 3*a*b*(1-3*v)/2    ,  4*a^2+2*b^2*(1-v) , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v) , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)    ,  3*a*b*(1+v)/2]
    kloc[4,:] = [-3*a*b*(1-3*v)/2     ,  2*b^2-2*a^2*(1-v)  , -3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2      , -2*b^2-a^2*(1-v)]
    kloc[5,:] = [-2*a^2-b^2*(1-v)     , -3*a*b*(1+v)/2      ,  2*a^2-2*b^2*(1-v) ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v) ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)    , -3*a*b*(1-3*v)/2]
    kloc[6,:] = [-3*a*b*(1+v)/2       , -2*b^2-a^2*(1-v)    , -3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2    ,  2*b^2-2*a^2*(1-v)]
    kloc[7,:] = [2*a^2-2*b^2*(1-v)   , -3*a*b*(1-3*v)/2    , -2*a^2-b^2*(1-v)   ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)   ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v)  , -3*a*b*(1+v)/2]
    kloc[8,:] = [3*a*b*(1-3*v)/2     , -4*b^2+a^2*(1-v)    ,  3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2      ,  4*b^2+2*a^2*(1-v)]


    ke = kloc *(Emod*h/(12*a*b*(1-v^2)))
    return ke
end;

function assembleK(nelx, nely, fac ,nD, tMat, loop)
    ke = stiffnessMatrix(fac)

    nN = (nelx + 1) * (nely + 1) * 2
    nE = nelx * nely

    if !(loop)
        t = tMat
        # duplicate 8-times number of nodes per element
        tx = repeat(t',8,1) #safe

        #build column vector
        colT = tx[:]
        #transpose
        tt = t' #safe
        #duplicate 8-times number of nodes per element
        tx = repeat(tt[:],1,8)' #safe
        #build row vector
        rowT = tx[:] #safe

        #duplicate number of elements-time
        knE = repeat(ke',1,nE)
        #build vector;
        keVec = knE[:]

        #build sparsed matrix
        # Pkg.add("stdlib")
        # Pkg.add("Linear Algebra
        dofs = (nelx + 1) * (nely + 1) * 2;
        k = sparse(rowT ,colT, keVec, dofs, dofs)
    else
        k = Array{Float64}(undef, nN, nN)
        for e = 1 : nE
            n = 8
            Te = tMat[e,:]'
            for i = 1:n
                for j = 1:n
                    I = Te[i]
                    J = Te[j]
                    k[I,J] = k[I,J] + ke[i,j]
                end
            end
        end
    end

    d = [nD[1] ; nD[2]]
    for i = 1:length(d)
        j = d[i]
        k[j,:] .= 0.0
        k[:,j] .= 0.0
        k[j,j] = 1.0
    end
    return k
end;

function assembleR(nelx, nely, nN)
    r = zeros((nelx+1)*(nely+1)*2,1)
    nload = copy(nN[2])
    f = 10
    fi = f/(length(nload))
    r[nload] .= -fi
    return r
end;


function solve(nelx, nely, fac, loop)
    nelx = 100; nely = 100
    nD = getDirichletNodes(nelx, nely)
    nN = getNeumannNodes(nelx, nely)
    tMat = getTMatrix(nelx, nely)
    k = assembleK(nelx, nely, fac ,nD, tMat, loop)
    r = assembleR(nelx, nely, nN)
    #uHat = cholesky(k)\r
    #uHat = lu(k)\r;
    uHat = k\r
    return uHat
end;
