include("Assemble.jl"); include("BCs.jl"); include("TMat.jl");
using Printf
using LinearAlgebra
using SparseArrays
using Statistics
function check(nelx, nely, rmin, x, dc)
  dcn = zeros(nely, nelx)
  radius = floor(Integer, rmin)

  for i = 1:nelx
    for j = 1:nely
      sum = 0.0

      for k = max(i - radius, 1):min(i + radius, nelx)
        for l = max(j - radius, 1):min(j + radius, nely)
          fac = rmin - sqrt((i - k)^2 + (j - l)^2)
          sum = sum + max(0, fac)
          dcn[j, i] = dcn[j, i] + max(0, fac) * x[l, k] * dc[l, k]
        end
      end

      dcn[j, i] = dcn[j, i] / (x[j, i] * sum)
    end
  end

  return dcn
end

function solve(nelx, nely, volfrac, penal)
  nD = getDirichletNodes(nelx, nely)
  nN = getNeumannNodes(nelx, nely)
  tMat = getTMatrix(nelx, nely)
  k = assembleK(nelx, nely ,nD, tMat, false, volfrac ,penal)
  r = assembleR(nelx, nely, nN)
  uHat = k\r
  return uHat
end

function getCompliance(nelx, nely, x, penal, u)
  dc = zeros(nely, nelx)
  ke = stiffnessMatrix(1)
  c = 0.0
  for ely = 1:nely
    for elx = 1:nelx
      n1 = (nely + 1) * (elx - 1) + ely
      n2 = (nely + 1) * elx + ely
      edof = [
        2n1 - 1
        2n1
        2n2 - 1
        2n2
        2n2 + 1
        2n2 + 2
        2n1 + 1
        2n1 + 2
      ]
      Ue = u[edof]
      compliance = (Ue' * ke * Ue)[1]
      c = c + x[ely, elx]^penal * compliance
      dc[ely, elx] = -penal * x[ely, elx]^(penal - 1) * compliance
    end
  end
  return c, dc
end

function top(nelx, nely, volfrac, penal, rmin)
  # INITIALIZE
  x = fill(volfrac, (nely, nelx))
  dc = zeros(nely, nelx)
  loop = 0
  change = 1.0

  # START ITERATION
  while change > 0.01
    loop += 1
    xold = x
    u  = solve(nelx, nely, volfrac, penal)
    c,dc = getCompliance(nelx, nely, x, penal, u)
    dc = check(nelx, nely, rmin, x, dc)
    if dc[1,1] < 0.0
     dc = fill(-1000000.1, (nely, nelx))
    end
    l1 = 0; l2 = 1e9; move = 0.2; xnew = 0
    while (l2-l1)/(l1+l2) > 1e-3
      lmid = 0.5*(l2+l1)
      d = -dc/lmid
      xnew = max.(0.001, max.(max.(x.- move), min.(1.00, min.(min.(x .+ move), min.(x.* sqrt.(d))))))
      if sum(sum(xnew)) - volfrac * nelx * nely > 0
        l1 = lmid
      else
        l2 = lmid
      end
    end
    change = maximum(abs.(xnew[:].-x[:]))
    x = xnew
    println(change)
    # print result
    @printf(" It.:%5i Obj.:%11.4f Vol.:%7.3f ch.:%7.3f\n",loop,c, sum(xnew[:]),change)
  end
end

nelx = 50; nely = 20; volfrac = 0.3; penal = 3;
top(nelx, nely, volfrac, penal, 3)
u  = solve(nelx, nely, volfrac, penal)
x  = fill(0.5, nely,nelx)
c,dc = getCompliance(nelx, nely, x, penal, u)


a = [1 2 3 4]
b = maximum(a[:])
print(b)
