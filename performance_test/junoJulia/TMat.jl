function getTVector(e, nelx, nely)
    nx = nelx
    i = e-1
    row = i/nx - mod((i/nx), 1)
    n = fill(Int64(row+i), 1,4)
    n[1:2] .+= 1:2
    n[3:4] .+= [nx+3,nx+2]

    # compute T-Vector
    Te = Array{Int64}(undef,8)
    # first sequence
    subtr = 0:3
    s1 = (1:4) + subtr.*2-subtr

    # second sequence
    s2 = (1:4)*2
    subtr = 2-1
    Te[s1] = n*2 .-subtr
    Te[s2] = n*2
    return Te
end;

function getTMatrix(nelx, nely)
    nE = nelx * nely
    t = Array{Int64}(undef,nE, 8) #zeros(nE,8);
    for e = 1:nE
        t[e,:] = getTVector(e, nelx, nely);
    end
    return t
end;
