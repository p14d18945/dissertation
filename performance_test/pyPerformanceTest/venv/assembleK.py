import numpy as np

def ke(fac):
    # Calculate Stiffnessmatrix for Quad4-Elements
    # Emod: young's modulus
    # h: thickness
    # a: elementhigh
    # b: elementwidth
    ex = np.power(10, 4)
    Emod = 210000 * ex
    v = 0.3
    a = 0.01 * fac
    b = 0.01 * fac
    h = 0.2

    ap = np.power(a, 2)
    bp = np.power(b, 2)
    vp = np.power(v, 2)
    ke = np.zeros((8, 8))

    ke[0] = [4 * ap + 2 * bp * (1 - v), 3 * a * b * (1 + v) / 2, -4 * ap + bp * (1 - v), -3 * a * b * (1 - 3 * v) / 2,
             -2 * ap - bp * (1 - v), -3 * a * b * (1 + v) / 2, 2 * ap - 2 * bp * (1 - v), 3 * a * b * (1 - 3 * v) / 2]
    ke[1] = [3 * a * b * (1 + v) / 2, 4 * bp + 2 * ap * (1 - v), 3 * a * b * (1 - 3 * v) / 2, 2 * bp - 2 * ap * (1 - v),
             -3 * a * b * (1 + v) / 2, -2 * bp - ap * (1 - v), -3 * a * b * (1 - 3 * v) / 2, -4 * bp + ap * (1 - v)]
    ke[2] = [-4 * ap + bp * (1 - v), 3 * a * b * (1 - 3 * v) / 2, 4 * ap + 2 * bp * (1 - v), -3 * a * b * (1 + v) / 2,
             2 * ap - 2 * bp * (1 - v), -3 * a * b * (1 - 3 * v) / 2, -2 * ap - bp * (1 - v), 3 * a * b * (1 + v) / 2]
    ke[3] = [-3 * a * b * (1 - 3 * v) / 2, 2 * bp - 2 * ap * (1 - v), -3 * a * b * (1 + v) / 2,
             4 * bp + 2 * ap * (1 - v), 3 * a * b * (1 - 3 * v) / 2, -4 * bp + ap * (1 - v), 3 * a * b * (1 + v) / 2,
             -2 * bp - ap * (1 - v)]
    ke[4] = [-2 * ap - bp * (1 - v), -3 * a * b * (1 + v) / 2, 2 * ap - 2 * bp * (1 - v), 3 * a * b * (1 - 3 * v) / 2,
             4 * ap + 2 * bp * (1 - v), 3 * a * b * (1 + v) / 2, -4 * ap + bp * (1 - v), -3 * a * b * (1 - 3 * v) / 2]
    ke[5] = [-3 * a * b * (1 + v) / 2, -2 * bp - ap * (1 - v), -3 * a * b * (1 - 3 * v) / 2, -4 * bp + ap * (1 - v),
             3 * a * b * (1 + v) / 2, 4 * bp + 2 * ap * (1 - v), 3 * a * b * (1 - 3 * v) / 2, 2 * bp - 2 * ap * (1 - v)]
    ke[6] = [2 * ap - 2 * bp * (1 - v), -3 * a * b * (1 - 3 * v) / 2, -2 * ap - bp * (1 - v), 3 * a * b * (1 + v) / 2,
             -4 * ap + bp * (1 - v), 3 * a * b * (1 - 3 * v) / 2, 4 * ap + 2 * bp * (1 - v), -3 * a * b * (1 + v) / 2]
    ke[7] = [3 * a * b * (1 - 3 * v) / 2, -4 * bp + ap * (1 - v), 3 * a * b * (1 + v) / 2, -2 * bp - ap * (1 - v),
             -3 * a * b * (1 - 3 * v) / 2, 2 * bp - 2 * ap * (1 - v), -3 * a * b * (1 + v) / 2,
             4 * bp + 2 * ap * (1 - v)]
    ke = ke * (Emod * h / (12 * a * b * (1 - vp)))

    return ke


def assembleK(nelx, nely, fac ,nD, tMat, loop):
    kloc = ke(fac)
    nN = (nelx + 1) * (nely + 1) * 2
    nE = nelx * nely
    k = np.zeros((nN, nN))
    n = 8
    for e in range(0, nE):
        Te = tMat[e]
        for i in range(0, n):
            for j in range(0, n):
                I = int(Te[i])
                J = int(Te[j])
                k[I - 1, J - 1] = k[I - 1, J - 1] + kloc[i, j]

    nD = np.reshape(nD, (int(nD.size), 1))
    for i in range(0, nD.size):
        j = int(nD[i])
        k[j - 1, :] = 0
        k[:, j - 1] = 0
        k[j - 1, j - 1] = 1

    return k