import scipy
from numba import jit
import numpy as np
#@jit(nopython=True) # Set "nopython" mode for best performance, equivalent to @njit
#import timeit
#code_to_test = """
#from scipy import sparse
from getTMatrix import TMatrix
from getBCNodes import getDirichletNodes
from getBCNodes import getNeumannNodes
from assembleK import assembleK


nelx = 3
nely = 3
fac = 10
nN = (nelx + 1) * (nely + 1) * 2
np.set_printoptions(edgeitems=nN)
nD = getDirichletNodes(nelx, nely)
tMat = TMatrix(nelx, nely)
k = assembleK(nelx,nely,1,nD,tMat,1)

r = np.zeros((nN,1))
nID = getNeumannNodes(nelx, nely)

f = 10
fi = f/(nID.size/2)

for i in range(0, int((nID.size/2))):
    iD = int(nID[(i,1)])
    r[iD-1] = fi

uHat = np.linalg.solve(k, r)
#"""
#elapsed_time = timeit.timeit(code_to_test, number=100)/100
#print(elapsed_time)

#np.savetxt('text.txt',k,fmt='%.6f')

print(uHat)