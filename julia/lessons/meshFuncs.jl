using Printf
using LinearAlgebra









m = Mesh(1.3, 2.1, 0.5)
hDir = getElementSize(m)
neDir = getNeDir(m)



h = initMesh(m)

function countNodes(m::Mesh)::Int64
 return 1
end

function countElements(m::Mesh)::Int64
 return 1
end

function getSize(m::Mesh)::Float64
 return 1.0
end

function getElementSize(m::Mesh)::Float64
 return 1.0
end

function getNodePosition(m::Mesh, i::Int64)::Array{Float64,2}
 return zeros(1,2)
end

ne = getNodePosition(m, 2)

m = Mesh(1, 2, 0.1)
