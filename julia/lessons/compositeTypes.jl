
# create a new struct which combines variables to composite types

# simple example
struct Point
    # attributes or fields x, y
    x
    y
end


# call constructor and instanate object p as an instance of type Point
p = Point(3.0, 4.0)

# get values
x = p.x
y = p.y

# expression p.y = 1.0 throws an error because structs are general immutable!

# create mutable struct
mutable struct MPoint
    x
    y
end

blank = MPoint(0.0, 0.0)

# now expressions like
blank.x = 1.0
# are allowed. Be careful, mutalbe structs are not efficient!

# instances as an attribute
struct Rectangle
    width
    height
    corner
end

origin = MPoint(0.0, 0.0)
box = Rectangle(100.0, 200.0, origin)

# instances as an function argument
function printPoint(p)
     println("($(p.x), $(p.y))")
end

printPoint(blank)

# functions can modify mutable struct objects
# first example
function movePoint!(p, dx, dy)
    p.x += dx
    p.y += dy
    nothing
end

origin = MPoint(0.0, 0.0)
println(origin)
movePoint!(origin, 1.0, 2.0)
println(origin)

# second example
function moveRectangle(rect, dx, dy)
    movePoint!(rect.corner, dx, dy)
end

moveRectangle(box, 1.0, 3.0)
println(box)

# instances as return values
function findCenter(rect)
    Point(rect.corner.x + rect.width/
    2, rect.corner.y + rect.height / 2)
end

center = findCenter(box)
println(center)

# deepcopy to duplicate any object
p1 = MPoint(3.0, 4.0)
# to copy infos this statement
p2 = p1
# is false, because p2 is the same object as p1
p1 == p2
# changes in p2 causes changes in p1
p2.x = 3
p1.x
# what we have to do is called copy or deepcopy
p2 = deepcopy(p1)
p1 == p2
p2.x = 4.0
p1.x
