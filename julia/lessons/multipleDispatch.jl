# create code that can operate on different types
# type declarations
# :: operator rhs of assignment
using LinearAlgebra
using Printf
#(1 + 2) :: Float64
# int + int = int
(1 + 2) :: Int64

# :: operator lhs of an assignment
function returnfloat()
    x :: Float64 = 100
    x
end
x = returnfloat()
typeof(x)

# type annotation attached to header of function
function sinFunc(x) :: Float64
    if x == 0
        return 1  # integer 1 will also be converted to Float64
    end
    sin(x)/x
end
y = sinFunc(0)
y = sinFunc(1)


# Methods
using Printf

struct MyTime
    hour :: Int64
    minute :: Int64
    second :: Int64
end

# function printTime takes any datatype
function printTime(time)
#    @printf("%02d:%02d:%02d",
#    time.hour, time.minute, time.second)
# redefine
    println("I don't know how to print the argument time.")
end

# function printTime takes only MyTime-arguments
function printTime(time::MyTime)
    @printf("%02d:%02d:%02d",
    time.hour, time.minute, time.second)
end

start = MyTime(11, 57, 0)
printTime(start)
printTime(10)

# create outter constructor Methods
function MyTime(time :: MyTime)
    MyTime(time.hour, time.minute, time.second)
end


# create inner constructor methods
struct MyTime
    hour :: Int64
    minute :: Int64
    second :: Int64
    function MyTime(hour::Int64=0,
        minute::Int64=0, second::Int64=0)
        @assert(0 <= minute < 60,
        "Minute is not between 0 and 60.")
        @assert(0 <= second < 60,
        "Second is not between 0 and 60.")
        # new has to be inside the block of a type
        # declaration and creates new objects
        new(hour, minute, second)
    end
end


# construct recursive data
mutable struct MyMutableTime
    hour :: Int
    minute :: Int
    second :: Int
    function MyMutableTime(hour::Int64=0,
        minute::Int64=0, second::Int64=0)
        @assert(0 <= minute < 60,
        "Minute is not between 0 and 60.")
        @assert(0 <= second < 60,
        "Second is not between 0 and 60.")
        time = new()
        time.hour = hour
        time.minute = minute
        time.second = second
        time
    end
end

# show-function
# returns a string representation of an object


function Base.show(io::IO,
    time::MyMutableTime)
    @printf(io, "%02d:%02d:%02d",
    time.hour, time.minute, time.second)
end

time = MyMutableTime(9, 45)

time = MyTime(1, 2, 3)



function timetoint(time :: MyTime)
    time.hour
    time.minute
    time.second    
end

# operator overloading
import Base.+
function +(t1::MyTime, t2::MyTime)
    seconds = timetoint(t1) +
    timetoint(t2)
    inttotime(seconds)
end

start = MyTime(9, 45)
duration = MyTime(1, 35, 0)
start + duration
