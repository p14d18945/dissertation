#
include("../optfunc/OptimizationMethod.jl")
using Makie
using AbstractPlotting
using Colors
using GeometryTypes
using GLMakie

mutable struct Visualizer
    # fe structure
    s :: Structure
    # fe mesh
    m :: Mesh
    # visualizeMesh
    showMesh :: Bool
    # userScale
    scale :: Float64
    # draw Loads
    drawLoads :: Bool
    # draw Constraints
    drawConstraints :: Bool
    # Optimizer
    o :: Optimizer



    function Visualizer(o :: Optimizer)
        s = o.top.s
        m = s.m
        showMesh = true
        scale = 1.0
        drawLoads = true
        drawConstraints = true
        return new(s, m, showMesh, scale, drawLoads, drawConstraints, o)
    end

    function Visualizer(s :: Structure)
        m = s.m
        showMesh = true
        scale = 1.0
        drawLoads = true
        drawConstraints = true
        return new(s, m, showMesh, scale, drawLoads, drawConstraints)
    end
end

function plot(v::Visualizer)
    connectivity = getConnectivityMatrix(s.m)
    convertedfaces = getFaces(connectivity)
    convertedColors = getColors(-s.x)
    coordinates = getCoordinatesMatrix(s.m)
    scene = mesh(coordinates, convertedfaces, color = convertedColors,
     colorrange = (-1.0, -0.2), colormap =:grayscale,
      shading = false, interpolate = false,  show_axis = false, scale_plot = false);

    if v.showMesh
        scene = drawWireframe(v, coordinates, scene);
    end

    if v.drawLoads
        scene = drawLoad(v, scene);
    end

    if v.drawConstraints
        scene = drawDirichlets(v, scene);
    end

    return scene
end

function plotf(v::Visualizer)
    # print time or number of iterations
    if v.o.plotFTime
        x = v.o.conTime
    else
        x = 0:v.o.nIt-1
    end

    y = v.o.cIt

    f_scene = lines(x, y, color = :blue)
    scatter!(f_scene, x, y, color = :red, markersize = 3.0*(1.5-v.o.nIt/100), show_axis = true)
    return f_scene
end

function drawWireframe(v::Visualizer, coordinates::Array{Float64,2}, scene::Scene) :: Scene
    # number of nodes
    nn = size(coordinates)[1]
    # number of elements
    # in x-direction
    nelx =  s.m.nelx
    # in y-direction
    nely =  s.m.nely

    # coords for horizontal lines
    x_horiz = Array{Float64, 2}(undef, nely+1, nelx+1)
    y_horiz = Array{Float64, 2}(undef, nely+1, nelx+1)
    iDs = Array{Int64, 2}(undef, nely+1, nelx+1)

    # fill horizontal coords
    for i = 1:nely+1
        # build up ID-matrix with number of Nodes
        iDs[i, :] = (i-1)*nelx+i*1:(i*nelx)+i*1
        if isodd(i)
            x_horiz[i, :] = coordinates[iDs[i, :], 1]
        else
            x_horiz[i, :] = reverse(coordinates[iDs[i, :], 1], dims = 1)
        end
        y_horiz[i, :] = coordinates[iDs[i, :], 2]
    end

    x_horiz = x_horiz'[:]
    y_horiz = y_horiz'[:]


    # coords for vertical lines
    x_vert = Vector{Float64}(undef, nn)
    y_vert = Vector{Float64}(undef, nn)
    vert_iDs = Array{Int64, 2}(undef, nely+1, nelx+1)

    for k = 1:nelx+1
        if isodd(k)
            vert_iDs[:,k] = iDs[:,k]
        else
            vert_iDs[:,k] = reverse(iDs[:,k], dims = 1)
        end
    end

    vert_iDs = vert_iDs[:]

    for i = 1:nn
        x_vert[i] = coordinates[vert_iDs[i], 1]
        y_vert[i] = coordinates[vert_iDs[i], 2]
    end

    # draw lines
    lines!(scene, x_horiz, y_horiz, color = :black)
    lines!(scene, x_vert, y_vert, color = :black)
    return scene
end

function getFaces(connectivity::Array{Int64, 2})
    n = size(connectivity)[1]
    convertedfaces = Array{Int64}(undef, 2*n, 3)
    for i = 1:n
        convertedfaces[2*i-1,:] = connectivity[i,[1 3 4]]
        convertedfaces[2*i,:] = connectivity[i,[1 2 3]]
    end
    return convertedfaces
end

function getColors(density::Array{Float64,2})
    (nely, nelx) = size(density)
    cMat = Array{Float64,2}(undef, nely+1, nelx+1)
    for i = 1:nely+1
        for j = 1:nelx+1
            if i == 1
                if j == 1
                    cMat[i,j] = density[1,1]
                elseif j > 1 && j < nelx+1
                    cMat[i,j] = (density[1,j-1] + density[1,j])/2
                elseif j == nelx+1
                    cMat[i,j] = density[1,nelx]
                end

            elseif i > 1 && i < nely+1
                if j == 1
                    cMat[i,j] = (density[i-1,1] + density[i,1])/2
                elseif j > 1 && j < nelx+1
                    cMat[i,j] = (density[i-1,j-1] + density[i-1,j] + density[i,j-1] + density[i,j])/4
                elseif j == nelx+1
                    cMat[i,j] = (density[i-1,nelx] + density[i,nelx])/2
                end
            elseif i == nely+1
                if j == 1
                    cMat[i,j] = density[nely,1]
                elseif j > 1 && j < nelx+1
                    cMat[i,j] = (density[nely,j-1] + density[nely,j])/2
                elseif j == nelx+1
                    cMat[i,j] = density[nely,nelx]
                end
            end
        end
    end
    convertedColors = cMat'[:]
    return convertedColors
end

##################  draw neumann conditions #########

function drawLoad(v::Visualizer, scene::Scene)::Scene
    # structure
    s = v.s
    for i = 1:length(s.neumannList)
        nBC = s.neumannList[i]
        if nBC.q[2] != 0
            zBC = Base.deepcopy(nBC)
            zBC.q[1] = 0
            scene = drawLoadZ(v, scene, zBC)
        end
        if nBC.q[1] != 0
            xBC = Base.deepcopy(nBC)
            xBC.q[2] = 0
            scene = drawLoadX(v, scene, xBC);
        end
    end
    return scene
end

function drawLoadX(v::Visualizer, scene::Scene, nBC::NeumannBC) :: Scene
    p1 = nBC.p1
    p2 = nBC.p2
    q = nBC.q

    # deflaut settings for cases
    # 1: horizontally load
    case_one = false
    # 2: vertically load
    case_two = false

    # distance between both points
    distX = p2[1]-p1[1]
    distZ = p2[2]-p1[2]


    m = abs(distZ/distX)
    deg = atand(m)

    if distX == 0 && distZ == 0
        deg = 1000
    end

    if deg < 85
        case_one = true
    elseif deg >= 85 || deg == 1000
        case_two = true
    end

    # compute Scales
    lScale = computeScale(v)
    eScale = computeEScale(v)[2]

    automaticScale =  (1/3.0*lScale)^2

    # space between structure and load
    space = Base.min(v.scale * automaticScale, Base.max(s.m.ly/7, s.m.lx/7))

    if case_one

        # minimal amount of arrows
        if abs(distX) >= s.m.eSize[1] && abs(distX) > s.m.lx/100
            minN = 2
        else
            minN = 1
        end

        # calculate arrows high
        ah = Base.min(Base.max(lScale,abs(Float64(q[1]))), s.m.ly/4.0, s.m.lx/4.0)
        # ah = z
        # factor to minimize arrowhigh if its smaller than neumannlength
        nFac = Base.min(distX/ah, 1.0)
        # minimal distance between arrows
        minDist = s.m.lx/5
        # number of arrows to remove
        rA = Base.max(Int64(round(minDist/ah)),1)
        # maximal amount of arrows
        maxN = Int64(round(nFac*distX/ah)-rA)
        # amount of arrows
        nArrows = Base.max(minN, maxN)
        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 1:nArrows
        # check if ah is smaller than distX
        ah = Base.min(ah, distX)

        # arrow direction
        dir = q[1]/abs(q[1])
        ah = ah*dir

        # calculate distance between arrows
        # complete arrowlength
        sum_al = nArrows * abs(ah)
        # distance between arrowlength and structure high
        delta = s.m.lx - sum_al
        smallDelta = delta/(nArrows-1)

        epsilon = 2*1e-2*sqrt(lScale)*v.scale

        # calculate x-coordinates
        if dir > 0
            xRange = Vector{Float64}(undef, nArrows)
            [xRange[i] = p1[1] + i*ah + (i-1)*smallDelta for i in 1:nArrows]#
        elseif dir < 0
            xRange = Vector{Float64}(undef, nArrows)
            [xRange[i] = p1[1] + (i-1)*abs(ah) + (i-1)*smallDelta for i in 1:nArrows]#
        end

        # calculate z-coordinates
        zRange = range(p1[2]+0.2*space, p2[2]+0.2*space,length=nArrows)

        # arrow direction
        z1 = -ah

        line = [Point2f0(xRange[i], zRange[i]) for i in arrowRange]
        dirs = fill(Point2f0(z1, 0), nArrows)

        # start drawing
        arrows!(scene,
            line,
            dirs,
            arrowcolor=:red,
            arrowsize=0.04*sqrt(lScale)*v.scale,
            linewidth = 1.5*sqrt(lScale)*v.scale,
            linecolor=:red
        );

    elseif case_two

        # calculate number of arrows
        if abs(distZ) >= s.m.eSize[2] && abs(distZ) > s.m.ly/100
            minN = 2
        else
            minN = 1
        end

        # number of arrows in plot
        maxN = Int64(round(distZ/eScale+1))
        nArrows = Base.max(minN, maxN)

        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 1:nArrows

        # calculate arrowhigh
        ah = Base.min(Base.max(lScale,abs(Float64(q[1]))), s.m.lx/3.0, s.m.ly/3.0)

        # arrow direction
        dir = q[1]/abs(q[1])
        ah = ah*dir
        #ah = z;
        # calculate z-coordinates
        zRange = range(p1[2], p2[2],length=nArrows)

        #calculate x-coordinates
        if dir > 0
            xStart = p1[1] + space + ah
            xEnd = p2[1] + space + ah
            xRange = range(xStart,xEnd,length=nArrows)
        elseif dir < 0
            xStart = p1[1] + space
            xEnd = p2[1] + space
            xRange = range(xStart,xEnd,length=nArrows)
        end

        # arrow direction
        z1 = -ah

        line = [Point2f0(xRange[i], zRange[i]) for i in arrowRange]
        dirs = fill(Point2f0(z1, 0), nArrows)

        # start drawing
        arrows!(scene,
            line,
            dirs,
            arrowcolor=:red,
            arrowsize=0.04*sqrt(lScale)*v.scale,
            linewidth = 1.5*sqrt(lScale)*v.scale,
            linecolor=:red
        );

        lines!(scene, [xRange[1], xRange[end]], [zRange[1], zRange[end]],
            color=:red,
            linewidth = 1.5*sqrt(lScale)*v.scale,
        );

    end
    return scene
end

function drawLoadZ(v::Visualizer, scene::Scene, nBC::NeumannBC) :: Scene
    p1 = nBC.p1
    p2 = nBC.p2
    q = nBC.q

    # deflaut settings for cases
    # 1: horizontally load
    case_one = false
    # 2: vertically load
    case_two = false

    # distance between both points
    distX = p2[1]-p1[1]
    distZ = p2[2]-p1[2]
    m = abs(distZ/distX)
    deg = atand(m)

    if distX == 0 && distZ == 0
        deg = 1000
    end

    if deg < 85 || deg == 1000
        case_one = true
    else
        case_two = true
    end

    # compute Scales
    lScale = computeScale(v)
    eScale = computeEScale(v)[1]

    automaticScale =  (1/3.0*lScale)^2

    # space between structure and load
    space = Base.min(v.scale * automaticScale, Base.max(s.m.ly/7, s.m.lx/7))

    if case_one

        # calculate number of arrows
        if abs(distX) >= s.m.eSize[1] && abs(distX) > s.m.lx/100
            minN = 2
        else
            minN = 1
        end

        # number of arrows in plot
        maxN = Int64(round(distX/eScale+1))
        nArrows = Base.max(minN, maxN)

        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 0:nArrows-1

        # calculate arrowhigh
        z = Base.min(Base.max(lScale,abs(Float64(q[2]))), s.m.ly/3.0, s.m.lx/3.0)

        # arrow direction
        dir = q[2]/abs(q[2])
        z = z*dir

        # calculate z-coordinates
        if dir > 0
            z0_Start = p1[2] + space + z
            z0_End = p2[2] + space + z
            z0 = range(z0_Start,z0_End,length=nArrows)

        elseif dir < 0
            z0_Start = p1[2] + space
            z0_End = p2[2] + space
            z0 = range(z0_Start,z0_End,length=nArrows)
        end

        # arrow direction
        z1 = -z

        line = [Point2f0(p1[1]+distX/(nR)*i, z0[i+1]) for i in arrowRange]
        dirs = fill(Point2f0(0, z1),nArrows)

        # start drawing
        arrows!(scene,
            line,
            dirs,
            arrowcolor=:red,
            arrowsize=0.04*sqrt(lScale)*v.scale,
            linewidth = 1.5*sqrt(lScale)*v.scale,
            linecolor=:red
        );

        lines!(scene, [p1[1], p2[1]], [z0[1], z0[end]],
            color=:red,
            linewidth = 1.5*sqrt(lScale)*v.scale,
        );

    elseif case_two

        # minimal amount of arrows
        if abs(distZ) >= s.m.eSize[2] && abs(distZ) > s.m.ly/100
            minN = 2
        else
            minN = 1
        end

        # calculate arrows high
        z = Base.min(Base.max(lScale,abs(Float64(q[2]))), s.m.ly/4.0, s.m.lx/4.0)
        # factor to mininize arrowhigh if its smaller than neumannlength
        nFac = Base.min(distZ/z, 1.0)
        # minimal distance between arrows
        minDist = s.m.ly/5
        # number of arrows to remove
        rA = Base.max(Int64(round(minDist/z)),1)
        # maximal amount of arrows
        maxN = Int64(round(nFac*distZ/z)-1)
        # amount of arrows
        nArrows = Base.max(minN, maxN)
        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 0:nArrows-1
        # check if z is smaller than distZ
        z = Base.min(z, distZ)
        # arrow direction
        dir = q[2]/abs(q[2])
        z = z*dir

        # calculate distance between arrows
        # complete arrowlength
        sum_al = nArrows * abs(z)
        # distance between arrowlength and structure high
        delta = s.m.ly - sum_al
        smallDelta = delta/(nArrows-1)

        epsilon = 2*1e-2*sqrt(lScale)*v.scale
        # calculate z-coordinates
        if dir > 0
            z0 = Vector{Float64}(undef, nArrows)
            [z0[i] = epsilon + p1[2] + i*z + (i-1)*smallDelta for i in 1:nArrows]#
        elseif dir < 0
            z0 = Vector{Float64}(undef, nArrows)
            [z0[i] = p1[2] + (i-1)*abs(z) + (i-1)*smallDelta for i in 1:nArrows]#
        end

        # arrow direction
        z1 = -z

        line = [Point2f0(p1[1]+distX/(nR)*i + 0.2*space, z0[i+1]) for i in arrowRange]
        dirs = fill(Point2f0(0, z1),nArrows)

        # start drawing
        arrows!(scene,
            line,
            dirs,
            arrowcolor=:red,
            arrowsize=0.04*sqrt(lScale)*v.scale,
            linewidth = 1.5*sqrt(lScale)*v.scale,
            linecolor=:red
        );
    end

    return scene
end
################# draw dirichlet conditions ###########
function drawTriangleZ(v::Visualizer, scene::Scene,  pos::Vector{Float64}, fixed :: Vector{Bool}, triangleUP = true) :: Scene
    if triangleUP
        dir = 1.0
    elseif !triangleUP
        dir = -1.0
    end

    s = v.s # structure

    es = computeEScale(v)
    scale = Base.max(0.6*sqrt(es[1]^2+es[2]^2), 0.1)


    if fixed[1] && !fixed[2] # only x-direction is fixed

        lineStart = [pos[1], pos[2]]
        lineEnd =  [pos[1], pos[2]-scale*1.6*dir]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);

        lineStart = [pos[1]-scale*0.2, pos[2]-scale*0.2*dir]
        lineEnd =  [pos[1]-scale*0.2, pos[2]-scale*1.6*dir]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);


        linelength = scale*1.4
        for i = 1:4
            lineStart = [pos[1]-scale*0.2, pos[2]-scale*0.3*dir-(i-1)*linelength/4*dir]
            lineEnd = [pos[1]-scale*0.4, pos[2]-scale*0.5*dir-(i-1)*linelength/4*dir]

            line = Point2f0[Point2(lineStart), Point2(lineEnd)]
            lines!(scene, line, color = :magenta, linewidth = 1.5);
        end
    elseif fixed[2] ## z-direction is fixed

        points = Point2f0[[pos[1]-scale*0.5, pos[2]-scale*1.0*dir], [pos[1]+scale*0.5,
            pos[2]-scale*1.0*dir], pos, [pos[1]-scale*0.5, pos[2]-scale*1.0*dir]]

        colors = [0.7 ,0.7, 0.7, 0.7]

        poly!(scene, points, color = colors, colorrange = (0.0,1.0),
            strokecolor = (:black, 0.6), strokewidth = 1);

        lineStart = [pos[1]-scale*0.7, pos[2]-scale*1.0*dir]
        lineEnd =  [pos[1]+scale*0.7, pos[2]-scale*1.0*dir]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);

        if fixed[1]
            linelength = scale*1.4
            for i = 1:4
                lineStart = [pos[1]-scale*0.5+(i-1)*linelength/4, pos[2]-scale*1.0*dir]
                lineEnd =  [pos[1]-scale*0.7+(i-1)*linelength/4, pos[2]-scale*1.3*dir]
                line = Point2f0[Point2(lineStart), Point2(lineEnd)]
                lines!(scene, line, color = :magenta, linewidth = 1.5);
            end
        end
    end
    return scene
end

function drawTriangleX(v::Visualizer, scene::Scene, pos::Vector{Float64}, fixed :: Vector{Bool}, triangleRIGHT = true) :: Scene
    if triangleRIGHT
        dir = 1.0
    elseif !triangleRIGHT
        dir = -1.0
    end

    s = v.s # structure

    es = computeEScale(v)
    scale = Base.max(0.6*sqrt(es[1]^2+es[2]^2), 0.1)


    if fixed[1] ## z-direction is fixed

        points = Point2f0[[pos[1]-scale*1.0*dir, pos[2]+scale*0.5], [pos[1]-scale*1.0*dir,
            pos[2]-scale*0.5], pos, [pos[1]-scale*1.0*dir, pos[2]+scale*0.5]]

        colors = [0.7 ,0.7, 0.7, 0.7]

        poly!(scene, points, color = colors, colorrange = (0.0,1.0),
            strokecolor = (:black, 0.6), strokewidth = 1);

        lineStart = [pos[1]-scale*1.0*dir, pos[2]+scale*0.7]
        lineEnd =  [pos[1]-scale*1.0*dir, pos[2]-scale*0.7]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);

        if fixed[2]
            linelength = scale*1.4
            for i = 1:4
                lineStart = [pos[1]-scale*1.0*dir, pos[2]-scale*0.5+(i-1)*linelength/4]
                lineEnd =  [pos[1]-scale*1.3*dir, pos[2]-scale*0.7+(i-1)*linelength/4]
                line = Point2f0[Point2(lineStart), Point2(lineEnd)]
                lines!(scene, line, color = :magenta, linewidth = 1.5);
            end
        end

    elseif !fixed[1] && fixed[2]  # only x-direction is fixed

        lineStart = [pos[1], pos[2]]
        lineEnd =  [pos[1]-scale*1.6*dir, pos[2]]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);

        lineStart = [pos[1]-scale*1.6*dir, pos[2]-scale*0.2]
        lineEnd =  [pos[1]-scale*0.2*dir, pos[2]-scale*0.2]
        line = Point2f0[Point2(lineStart), Point2(lineEnd)]
        lines!(scene, line, color = :magenta, linewidth = 1.5);


        linelength = scale*1.4
        for i = 1:4

            lineStart = [pos[1]-scale*1.4*dir+(i-1)*linelength/4*dir, pos[2]-scale*0.2]
            lineEnd =  [pos[1]-scale*1.6*dir+(i-1)*linelength/4*dir, pos[2]-scale*0.5]

            line = Point2f0[Point2(lineStart), Point2(lineEnd)]
            lines!(scene, line, color = :magenta, linewidth = 1.5);
        end
    end
    return scene
end

function drawDirichlets(v::Visualizer, scene::Scene, dBC::DirichletBC) :: Scene
    p1 = dBC.p1
    p2 = dBC.p2
    fixed = dBC.fixed

    # deflaut settings for cases
    # 1: horizontally fixed
    case_one = false
    # 2: vertically fixed
    case_two = false

    # distance between both points
    distX = p2[1]-p1[1]
    distZ = p2[2]-p1[2]


    m = abs(distZ/distX)
    deg = atand(m)
    if distX == 0 && distZ == 0
        deg = 1000
    end

    if deg < 85 || deg == 1000
        case_one = true
    else
        case_two = true
    end

    # compute Scales
    lScale = computeScale(v)


    automaticScale =  (1/3.0*lScale)^2

    lines!(scene, [p1[1], p2[1]], [p1[2], p2[2]],
        color=:green,
        linewidth = 1.5*sqrt(lScale)*v.scale,
    );

    if case_one
        # minimal amount of triangles
        if abs(distX) >= s.m.eSize[1] && abs(distX) > s.m.lx/100
            minN = 2
        else
            minN = 1
        end

        # element scale in X-DIR, limit is 0.15
        eScale = Base.max(computeEScale(v)[1], 0.15)
        # number of arrows in plot
        maxN = Int64(round(distX/eScale+1))
        nArrows = Base.max(minN, maxN)

        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 1:nArrows

        xRange = range(p1[1],p2[1],length=nArrows)
        zRange = range(p1[2],p2[2],length=nArrows)


        # check if triangles are in the first elementrow
        if distZ == 0 && zRange[1] == s.m.ly
            triangleUP = false
        else
            triangleUP = true
        end

        for i =arrowRange
            scene = drawTriangleZ(v, scene, [xRange[i], zRange[i]], fixed,triangleUP);
        end

    elseif case_two
        # minimal amount of triangles
        if abs(distZ) >= s.m.eSize[2] && abs(distZ) > s.m.ly/100
            minN = 2
        else
            minN = 1
        end

        # element scale in X-DIR, limit is 0.15
        eScale = Base.max(computeEScale(v)[2], 0.15)
        # number of arrows in plot
        maxN = Int64(round(distZ/eScale+1))
        nArrows = Base.max(minN, maxN)

        # drawing range
        nR = max(nArrows-1,1)
        arrowRange = 1:nArrows

        xRange = range(p1[1],p2[1],length=nArrows)
        zRange = range(p1[2],p2[2],length=nArrows)


        # check if triangles are in the last elementcolumn
        if distX == 0 && xRange[1] == s.m.lx
            triangleRIGHT = false
        else
            triangleRIGHT = true
        end

        for i =arrowRange
            scene = drawTriangleX(v, scene, [xRange[i], zRange[i]], fixed,triangleRIGHT);
        end

    end
    return scene
end


function drawDirichlets(v::Visualizer, scene::Scene)::Scene
    # MULTIBLE DISPATCH
    # structure
    s = v.s
    for i = 1:length(s.dirichletList)
        dBC = s.dirichletList[i]
        scene = drawDirichlets(v, scene, dBC);
    end
    return scene
end
##################  scaling-functions  ###############

function computeScale(v::Visualizer)::Float64
    lx = v.s.m.lx
    ly = v.s.m.ly

    fac =  0.4 * sqrt(lx^2 + ly^2)
    return fac
end

function computeEScale(v::Visualizer)::Vector{Float64}
    lx = v.s.m.lx
    lz = v.s.m.ly

    e = v.s.m.eSize

    lxfac = e[1]
    lzfac = e[2]

    if lx/lxfac > 50 || lxfac < 0.05
        lxfac = 0.04
    end

    if lz/lzfac > 50  || lzfac < 0.05
        lzfac = 0.04
    end

    efac = [lxfac, lzfac];

    return efac
end
