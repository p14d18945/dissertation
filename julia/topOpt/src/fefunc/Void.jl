using Printf
using LinearAlgebra
include("Mesh.jl")

abstract type Void end

function getElementIDs(v::subvoid, m::Mesh) where subvoid<:Void
    nE = countElements(m)
    ids = fill(false,nE)
    for i = 1:nE
        p = getElementCenter(m, i);
        ids[i]  =  isInside(v, p);
    end
    return ids
end
