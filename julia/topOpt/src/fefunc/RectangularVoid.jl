using Printf
include("Void.jl")

struct RectangularVoid <: Void
   # p1(x,y): left bottomcorner-point
   # p2(x,y): right topcorner-point
   p1::Vector{<:Number}
   p2::Vector{<:Number}
   # inner constructor
   RectangularVoid(p1, p2) = new(p1, p2)

   # define show-function
   function Base.show(io::IO,
      rv::RectangularVoid)
      @printf(io, "[%0.2f,%0.2f], [%0.2f,%0.2f]",
      rv.p1..., rv.p2...)
   end
end

function isInside(rv::RectangularVoid, p::Vector{<:Number})::Bool
   x = p[1]
   y = p[2]
   if x < rv.p2[1] && x > rv.p1[1] && y < rv.p2[2] && y > rv.p1[2]
      b = true
   else
      b = false
   end
   return b
end

#### HOW TO USE? ####
# m = Mesh(3,3,0.5)
# init(m)
# rv = RectangularVoid([1,0], [2,1])
# ids = getElementIDs(rv, m)
# println(ids)
