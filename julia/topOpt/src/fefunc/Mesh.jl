using Printf
using LinearAlgebra

mutable struct Mesh
    lx ::Float64
    ly :: Float64
    h :: Float64
    eSize :: Vector{Float64}
    nelx :: Int64
    nely :: Int64

    # inner constructor
    Mesh(lx, ly, h) = new(lx, ly, h)
    #m = Mesh
    # define show-function
    function Base.show(io::IO,
     m::Mesh)
        @printf(io, "%0.2f, %0.2f, %0.2f",
        m.lx, m.ly, m.h)
    end
end

function init(m :: Mesh)
    lx = m.lx
    ly = m.ly
    hTry = m.h

    # check if domainsize is possible w. choosed ElementSize
    # if not, calc. rest of division
    rx = mod((1/hTry)*lx, 1)
    ry = mod((1/hTry*ly), 1)

    # calc. elementsize in x- and y-direction
    hx  = (rx * hTry)/(1/hTry * lx-rx)+hTry
    hy  = (ry * hTry)/(1/hTry * ly-ry)+hTry
    m.eSize = [hx, hy]

    # number of elements in x-direction and y-direction
    m.nelx = Int16(round(lx / hx))
    m.nely = Int16(round(ly / hy))

    return [hx, hy], m.nelx, m.nely
end

function countElements(m::Mesh)::Int64
    nE = m.nelx*m.nely
    return nE
end

function countNodes(m::Mesh)::Int64
    nN = (m.nelx+1)*(m.nely+1)
    return nN
end

function getElementNodes(m::Mesh, e::Int64)::Vector{Int64}
    # e :: ElementID
    # compute Elementnodes of current Element
    nx = m.nelx
    i = e-1
    row  = i/nx - mod((i/nx), 1)
    n = Array{Int64}(undef, 4)
    n[1:2] .= row + i .+ [1,2]
    n[3:4] .= row + i .+ [nx+3,nx+2]
    return n
end

function getElementSize(m::Mesh)::Vector{Float64}
    return m.eSize
end

function getSize(m::Mesh)::Vector{Float64}
    return [m.lx, m.ly]
end

function getElementsInDirection(m::Mesh)::Vector{Float64}
    return [m.nelx, m.nely]
end

function getTVector(m::Mesh, e::Int64, nf=2)
    # e :: ElementID
    # nf :: DOFs per node
    nodes = getElementNodes(m, e)
    # compute T-Vector
    nn =  length(nodes)
    Te = Array{Int64}(undef, nf * nn)
    # first sequence
    s1 = Array{Int64}(ones(1,4) + (0:3)'.*nf)
    # second sequence
    s2 = (1:4)'.*nf
    subtr = nf-1
    Te[s1] = nodes*nf.-subtr
    Te[s2] = nodes*nf
    return Te
end


function getTMatrix(m::Mesh)
    nE = countElements(m)
    nF = length(getTVector(m,1))
    t = Array{Int64}(undef,nE,nF)
    for e = 1:nE
        tVec = getTVector(m, e)
        t[e,:] .= tVec
    end
    return t
end

function getMatrixInfos(m::Mesh, i::Int64, e::Bool)::Vector{Int64}
    # calculates the row and column of the mesh-matrix
    # for the i'th element or node
    # e: boolean
    #       true = element, calc. with number of Elements
    #       false = node, calc. with number of Nodes

    if !e
        k = 1
    else
        k = 0
    end

    # number of nodes or elements in x-direction
    nx = m.nelx + k


    # rest after division
    rx  = mod(i/nx, 1)

    # filtering row for last node in column
    if rx > 0
        row = i/nx - rx + 1
    else
        row = i/nx
    end
    column = i - (row-1) * nx
    info = [column, row]
    return Array{Int64}(info)
end

function getNodePosition(m::Mesh, i::Int64)::Vector{Float64}
   # compute the position for node i
   # eX  : elementsize in x-direction
   # eY  : elementsize in y-direction
   # p   : position (x,y)
   eX = m.eSize[1]
   eY = m.eSize[2]
   # compute row and column for i
   info = getMatrixInfos(m, i, false)
   column = info[1]
   row = info[2]
   p = [(column-1) * eX, (row-1) * eY]
   return p
 end

 function getElementCenter(m::Mesh, i::Int64)::Vector{Float64}
     # computes the center of the i'th element
     # eX  : elementsize in x-direction
     # eY  : elementsize in y-direction
     # p   : position (x,y)
     eX = m.eSize[1]
     eY = m.eSize[2]
     # compute row and column for i
     info = getMatrixInfos(m, i, true)
     column = info[1]
     row = info[2]
     c = [(column-1) * eX + eX/2, (row-1) * eY + eY/2]
     return c
 end


function getConnectivityMatrix(m::Mesh) :: Array{Int64,2}
    ne = countElements(m)
    cMatrix = Array{Int64}(undef, ne, 4)
    for e = 1:ne
        cMatrix[e,:] .= getElementNodes(m, e)
    end
    return cMatrix
end

function getCoordinatesMatrix(m::Mesh) :: Array{Float64,2}
    nn = countNodes(m)
    cordMatrix = Array{Float64}(undef, nn, 2)
    for n = 1:nn
        cordMatrix[n,:] .= getNodePosition(m, n)
    end
    return cordMatrix
end
