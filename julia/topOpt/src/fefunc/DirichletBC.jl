include("Mesh.jl")
include("BoundaryCondition.jl")

mutable struct DirichletBC <: BoundaryCondition
    # p1 : startPoint
    # p2 : endPoint
    p1::Vector{Float64}
    p2::Vector{Float64}
    # fixed direction
    fixed :: Vector{Bool}
    # inner constructor
    DirichletBC(p1, p2) = new(p1, p2)
end

function setFixed(dc :: DirichletBC, fixed :: Vector{Bool})
    dc.fixed = fixed
end

function getFixed(dc :: DirichletBC)
    return dc.fixed
end

function getDOFS(dc::DirichletBC, m::Mesh)::Vector{Bool}
    # get number of nodes
    nN = countNodes(m)
    # initialize DOF-matrix
    dofs = fill(false,2*nN)

    for i = 1:nN
        # IDs for vertical and horizontal DOFs
        # example:
        #          dir = [0, 1], node=2
        #          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
        id = dc.fixed.*[(i*2-1), i*2]

        # get specific node position
        p = getNodePosition(m, i)
        # check if node is on boundary, set dof = true/ false;
        dofs[id[id.>0]] .= isOnBoundary(dc, p)
    end
    return dofs
end

# HOW TO USE?
# m = Mesh(1,1,0.5)
# init(m)
# dc = DirichletBC([0,0],[0.5,0])
# fixed = [true, false]
# setFixed(dc, fixed)
# dofs = getDOFS(dc, m)
# print(r)
