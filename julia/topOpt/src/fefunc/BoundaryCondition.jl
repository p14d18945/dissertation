using Printf
using LinearAlgebra
include("Mesh.jl")

# superType
abstract type BoundaryCondition end

function isOnBoundary(sbc::subBC , p::Vector{<:Number}) where subBC <: BoundaryCondition
    # check if point is on Boundaryline
    # Input:
    # p1 : startPoint of BoundaryLine
    # p2 : endPoint of BoundaryLine
    # p : testPoint
    # output b: boolean

    x = [sbc.p1[1], sbc.p2[1]]
    y = [sbc.p1[2], sbc.p2[2]]
    # acceptable difference
    diff = 1e-4

    if (x[1]-diff <= p[1] && p[1] <= x[2]+diff)  && ((y[1]-diff <= p[2] && p[2]<= y[2]+diff)||(y[2]-diff <= p[2] && p[2]<= y[1]+diff))
        b = true
    else
        b = false
    end
end
