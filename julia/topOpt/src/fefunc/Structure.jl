# include files
include("Mesh.jl")
include("BoundaryCondition.jl")
include("DirichletBC.jl")
include("NeumannBC.jl")
include("Void.jl")
include("RectangularVoid.jl")
include("CircularVoid.jl")

using Printf
using LinearAlgebra
using SparseArrays

# überlegung Structure -> Model
# function Structure(lx, ly, h)
#   m = Model(lx, ly, h)
#   init(m)
#   return m
# end
######## main.jl ########
# s = Structure(lx, ly, h)
#
#########################

mutable struct Structure
    ##### Attributes ####
    # lx : size in x-direction
    lx::Float32
    # ly : size in y-direction
    ly::Float32
    # h : elementsize
    h::Float32
    # m : mesh-object
    m::Mesh
    # density field
    x::Array{Float64,2}
    # r : load-Vector
    r::Vector{Float64}
    # degrees of freedom
    dofs::Vector{Bool}
    # voidIDs : voidID-Vector
    voidIDs::Vector{Bool}
    # dirichlet counter
    cntDir::Int64
    # dirichletList
    dirichletList::Vector{DirichletBC}
    # neumann counter
    cntNeumn::Int64
    # dirichletList
    neumannList::Vector{NeumannBC}
    # possions ration
    mu :: Float64
    # element stiffnessMatrix
    ke :: Array{Float64,2}
    # rho defined boolean
    rhoDef::Bool
    # rho field
    rho::Array{Float64,2}
    # neumann entries
    nLoadVectors :: Array{<:Number,2}
    # dirchilet entries
    nDOFs :: Array{Bool,2}
    # inner constructor
    function Structure(lx, ly, h)
        m = Mesh(lx, ly, h)
        # initlialize mesh
        init(m)
        # redefinition of elementsize
        h = m.h
        # countElements to init voidIDs
        nE = countElements(m)
        # count nodes
        nN = countNodes(m)
        # init density field
        x = ones(m.nely, m.nelx)
        # init load vector
        r = zeros(2*nN)
        # init dofs
        dofs = fill(false, 2*nN)
        # init void elements
        voidIDs = fill(false, nE)
        # init cntDir
        cntDir = 0
        # init dirichletList
        dirichletList = Vector{DirichletBC}()
        # init cntNeumann
        cntNeumn = 0
        # init neumannList
        neumannList = Vector{NeumannBC}()
        # init mu
        mu = 0.3
        # init stiffnessMatrix
        ke = stiffnessMatrix(m.eSize[1], m.eSize[2], mu)
        # rho defined boolean
        rhoDef = false
        return  new(lx, ly, h, m, x, r, dofs, voidIDs, cntDir, dirichletList, cntNeumn, neumannList, mu, ke, rhoDef)
    end
end

function addVoid(s::Structure, v::subvoid) where subvoid<:Void
    voidIDs = findall(getElementIDs(v, s.m))
    for i = voidIDs
        if !s.voidIDs[i]
            s.voidIDs[voidIDs] .= true
        end
    end
    return Nothing
end

function addDirichletBC(s::Structure, p1::Vector{<:Number}, p2::Vector{<:Number}, fixed::Vector{Bool}) :: DirichletBC
    # update dirichlet counter
    s.cntDir += 1
    # initialize BoundaryCondition-Object
    dBC = DirichletBC(p1,p2)
    # set fixed direction
    setFixed(dBC, fixed)
    # save current dirichlet objects
    push!(s.dirichletList, dBC)
    return dBC
end

function addNeumannBC(s::Structure, p1::Vector{<:Number}, p2::Vector{<:Number}, value::Vector{<:Number}) :: NeumannBC
    # update dirichlet counter
    s.cntNeumn += 1
    # initialize BoundaryCondition-Object
    nBC = NeumannBC(p1,p2)
    # set fixed direction
    setValues(nBC, value)
    # save current dirichlet objects
    push!(s.neumannList, nBC)
    return nBC
end

function setUP(s::Structure)
    # number of nodes
    nN = countNodes(s.m)
    # mesh-object
    mesh = s.m
    # assemble load vector
    if !isempty(s.neumannList)
        # number of neumann objects
        nObj = length(s.neumannList)
        # init nLoadVectors
        s.nLoadVectors = zeros(2*nN, nObj)
        for i = 1:nObj
            # init current load Vector
            rStep = zeros(2*nN)
            # current neumann object
            nBC = s.neumannList[i]
            # current values
            q = getValues(nBC)
            for j = 1:Int64(length(rStep)/2)
                # IDs for vertical and horizontal DOFs
                 # example:
                 #          dir = [0, 1], node=2
                 #          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]

                id = [(j*2-1), j*2]

                # get specific node position
                pos = getNodePosition(mesh, j)
                if isOnBoundary(nBC, pos)
                    rStep[id[1]] = q[1]
                    rStep[id[2]] = q[2]
                end
            end
            # save different entries for plot
            s.nLoadVectors[:, i] .= rStep
            # update global load vector
            s.r += rStep
        end
    end

    if !isempty(s.dirichletList)
        # number of neumann objects
        nObj = length(s.dirichletList)
        # init nLoadVectors
        s.nDOFs = fill(false, 2*nN, nObj)
        for i = 1:nObj
            # init current load Vector
            dofsStep = fill(false, 2*nN)
            # current neumann object
            dBC = s.dirichletList[i]
            # current values
            dir = getFixed(dBC)

            for j = 1:Int64(length(dofsStep)/2)
                # IDs for vertical and horizontal DOFs
                 # example:
                 #          dir = [0, 1], node=2
                 #          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]

                id = dir.*[(j*2-1), j*2]

                # get specific node position
                pos = getNodePosition(mesh, j)
                # check if node is on boundary, set dof = true/ false;
                dofsStep[id[id.>0]] .= isOnBoundary(dBC, pos)
            end
            # save different entries for plot
            s.nDOFs[:, i] .= dofsStep
            # update global load vector
            #s.dofs(find(dofsStep),1) = true;
            #s.dofs = rStep
            s.dofs[findall(dofsStep)] .= true
        end
    end
    # find void Elements
    voidIDs = findall(s.voidIDs)
    # transpose density field because julia
    # fills matrix collumnwise
    xVoid = s.x'
    # set void elements to min density
    xVoid[voidIDs] .= 1e-5
    # set current density field
    s.x = xVoid'
    return Nothing
end

function setPossionsRatio(s::Structure, mu::Float64)
    s.mu = mu
    return Nothing
end

function setDensityMesh(s::Structure, x::Array{<:Number,2})
    # insert density mesh to update the structure
    s.x = x
    return Nothing
end

function setRhoMesh(s::Structure, rho::Array{<:Number,2})
    # insert rho mesh to update the structure
    s.rhoDef = true
    s.rho = rho
    return Nothing
end

function getDensityMesh(s::Structure)::Array{Float64,2}
    # return density mesh
    return s.x
end

function getRhoMesh(s::Structure)::Array{Float64,2}
    # return rho mesh
    if s.rhoDef
        rho = s.rho
    else
        rho = s.x
    end
end


function getNeighbourElements(s::Structure, xC::Array{Float64,2})
    # number of elements
    nE = countElements(s.m)
    # number of elements in both directions
    (nely, nelx) = [s.m.nely, s.m.nelx]
    # initialize
    init = zeros(nely,nelx)
    xN = copy(init)
    xE = copy(init)
    xS = copy(init)
    xW = copy(init)

    # ID Arrays
    elemMat = reshape(1:nE, nelx, nely)'
    idNorth = elemMat[1:nely-1,1:nelx]
    idEast =   elemMat[1:nely,2:nelx]
    idSouth =   elemMat[2:nely,1:nelx]
    idWest =   elemMat[1:nely,1:nelx-1]

    xTranspose = xC'

    # Arrays
    xN[2:nely,1:nelx] = xTranspose[idNorth]
    xS[1:nely-1,1:nelx] = xTranspose[idSouth]
    xE[1:nely,1:nelx-1] = xTranspose[idEast]
    xW[1:nely,2:nelx]  = xTranspose[idWest]
    return (xN, xS, xE, xW)
end

function getDensityLaplacian(s::Structure)::Array{Float64,2}
    # elementsize
    eS = s.m.eSize

    # DERIV-PART
    xC = s.x

    # NeighbourElements
    (xNorth, xSouth, xEast, xWest)=getNeighbourElements(s, xC)

    # Laplacian
    # finite difference scheme laplacian (3 Points)
    ddW = 2*(xWest-xC)/(2*eS[1]^2)
    ddE = 2*(xEast-xC)/(2*(eS[1])^2)
    ddN = 2*(xNorth-xC)/(2*eS[2]^2)
    ddS = 2*(xSouth-xC)/(2*(eS[2])^2)

    # apply neumann conditions
    ddW[:,1] .= 0
    ddE[:,end] .= 0
    ddN[1,:] .= 0
    ddS[end,:] .= 0

    # compute laplacian
    dd1 = ddE+ddW
    dd2 = ddN+ddS
    laplacianX = dd1+dd2.+1e-5
end

function stiffnessMatrix(a, b, mu)::Array{Float64,2}
    # Calculate Stiffnessmatrix for Quad4-Elements
    # Emod: young's modulus
    # h: thickness
    # a: elementhigh
    # b: elementwidth
    #Emod = 210000*10^4
    Emod = 1
    v = mu
    #a = s.m.eSize[1]
    #b = s.m.eSize[2]
    h = 1

    kloc = Array{Float64}(undef, 8, 8)
    kloc[1,:] = [4*a^2+2*b^2*(1-v) , 3*a*b*(1+v)/2 , -4*a^2+b^2*(1-v) , -3*a*b*(1-3*v)/2 , -2*a^2-b^2*(1-v) , -3*a*b*(1+v)/2 , 2*a^2-2*b^2*(1-v) , 3*a*b*(1-3*v)/2]
    kloc[2,:] = [3*a*b*(1+v)/2 , 4*b^2+2*a^2*(1-v) , 3*a*b*(1-3*v)/2 ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2    , -4*b^2+a^2*(1-v)]
    kloc[3,:] = [-4*a^2+b^2*(1-v) , 3*a*b*(1-3*v)/2    ,  4*a^2+2*b^2*(1-v) , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v) , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)    ,  3*a*b*(1+v)/2]
    kloc[4,:] = [-3*a*b*(1-3*v)/2     ,  2*b^2-2*a^2*(1-v)  , -3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2      , -2*b^2-a^2*(1-v)]
    kloc[5,:] = [-2*a^2-b^2*(1-v)     , -3*a*b*(1+v)/2      ,  2*a^2-2*b^2*(1-v) ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v) ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)    , -3*a*b*(1-3*v)/2]
    kloc[6,:] = [-3*a*b*(1+v)/2       , -2*b^2-a^2*(1-v)    , -3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2    ,  2*b^2-2*a^2*(1-v)]
    kloc[7,:] = [2*a^2-2*b^2*(1-v)   , -3*a*b*(1-3*v)/2    , -2*a^2-b^2*(1-v)   ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)   ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v)  , -3*a*b*(1+v)/2]
    kloc[8,:] = [3*a*b*(1-3*v)/2     , -4*b^2+a^2*(1-v)    ,  3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2      ,  4*b^2+2*a^2*(1-v)]


    ke = kloc *(Emod*h/(12*a*b*(1-v^2)))
    return ke
end

function assembleK(s::Structure)
    # number of elements in both directions
    (nely, nelx) = [s.m.nely, s.m.nelx]
    # elementstiffness MATRIX
    ke = s.ke
    # number of nodes
    nN = countNodes(s.m)
    # number of elements
    nE = countElements(s.m)
    # density mesh
    x  = s.x
    # t matrix
    t = getTMatrix(s.m)
    # duplicate 8-times number of nodes per element
    tx = repeat(t',8,1) #safe

    #build column vector
    colT = tx[:]
    #transpose
    tt = t' #safe
    #duplicate 8-times number of nodes per element
    tx = repeat(tt[:],1,8)' #safe
    #build row vector
    rowT = tx[:] #safe

    #duplicate number of elements-time
    knE = repeat(ke',1,nE)
    #build vector;
    keVec = knE[:]

    rho = getRhoMesh(s)
    # Transpose
    volTrans = rho'
    # duplicate 8x8-times elementstiffness size
    volNodal = repeat(volTrans[:],1,64)'
    # build Vector
    volVec = volNodal[:]
    # calc.
    density = volVec.*keVec
    #build sparsed matrix
    k = sparse(rowT ,colT, density, nN*2, nN*2)

    d = findall(s.dofs)

    if isempty(d)
        error("No fixed nodes, check Dirichlet-BCs!")
    end

    for i = 1:length(d)
        j = d[i]
        k[j,:] .= 0.0
        k[:,j] .= 0.0
        k[j,j] = 1.0
    end
    return k
end

function computeDisplacement(s::Structure)::Vector{Float64}
    # assembleK
    k = assembleK(s)
    # load Vector
    r = s.r
    # check if load vector exists
    if isempty(findall(abs.(r).>0))
        error("Loadvector is empty, check Neumann-BCs!")
    end
    # compute displacementVector
    u = k\r
end


function solve(s::Structure)
    # displacementVector
    u = computeDisplacement(s)
    # number of elements in both directions
    (nely, nelx) = [s.m.nely, s.m.nelx]
    # t matrix
    t = getTMatrix(s.m)
    # elementstiffness MATRIX
    ke = s.ke
    # get Rho-MESH
    rho = getRhoMesh(s)
    # calc. compliance factor
    aC = u[t]*s.ke.*u[t]
    # build compliance matrix
    cMat = reshape(sum(aC,dims=2),s.m.nelx, s.m.nely)'
    # compute piecewise compliance
    aCe = rho.*cMat
    # summarize all entries to get structures compliance
    c = 1/2 * sum(sum(aCe,dims=2))
    # gradient of compliance
    gradC = 1/2 * cMat
    return (c, gradC)
end
# assemble K
