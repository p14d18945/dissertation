include("Mesh.jl")
include("BoundaryCondition.jl")

mutable struct NeumannBC <:BoundaryCondition
    # p1 : startPoint
    # p2 : endPoint
    p1::Vector{Float64}
    p2::Vector{Float64}
    # load in direction
    q::Vector{<:Number}
    # inner constructor
    NeumannBC(p1, p2) = new(p1, p2)
end

function setValues(nc::NeumannBC, q::Vector{<:Number})
    nc.q = q;
end

function getValues(nc::NeumannBC)
    return nc.q
end

function  getR(nc :: NeumannBC, m::Mesh)::Vector{<:Number}
    # get number of nodes
    nN = countNodes(m)
    # initialize DOF-matrix
    r = zeros(2*nN)
    for i = 1:nN
        # IDs for vertical and horizontal DOFs
        # example:
        #          dir = [0, 1], node=2
        #          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
        id = [(i*2-1), i*2]
        # get specific node position
        p = getNodePosition(m, i)
        if isOnBoundary(nc, p)
            # check if node is on boundary, set dof = true/ false;
            r[id[1]] +=  nc.q[1]
            r[id[2]] +=  nc.q[2]
        end
    end
    return r
end

# HOW TO USE?
# m = Mesh(1,1,0.5)
# init(m)
# nc = NeumannBC([0,0],[0.5,0])
# setValues(nc, [4,3])
# r = getR(nc, m)
# print(r)
