include("Void.jl")
using Printf
using LinearAlgebra

struct CircularVoid <: Void
   # c(x,y): circular-point
   c::Vector{<:Number}
   # r : radius
   r
   # inner constructor
   CircularVoid(c,r) = new(c,r)

   # define show-function
   function Base.show(io::IO,
      cv::CircularVoid)
      @printf(io, "[%0.2f,%0.2f], %0.2f",
      cv.c...,cv.r...)
   end
end

function isInside(cv::CircularVoid, p::Vector{<:Number})::Bool
   x = p[1]
   y = p[2]
   if sqrt((y-cv.c[2])^2+(x-cv.c[1])^2) < cv.r
      b = true
   else
      b = false
   end
   return b
end

#### HOW TO USE? ####
# m = Mesh(3,3,0.5)
# init(m)
# cv = CircularVoid([0.5,0.5], 1)
# ids = getElementIDs(cv, m)
# println(ids)
