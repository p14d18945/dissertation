include("../fefunc/Structure.jl")

function getMBBeam(h=0.05)::Structure
    s = Structure(3,1,h)
    addDirichletBC(s, [3,0],[3,0],[false,true])
    addDirichletBC(s, [0,0],[0,1],[true,false])
    addNeumannBC(s,[0,1],[0,1],[0,1])
    setUP(s)
    solve(s)
    return s
end

function getSimpleBeam(h=0.025)::Structure
    ly = 0.4
    lx = 4*ly

    s = Structure(lx,ly,h)
    addDirichletBC(s, [0,0],[0,0],[true,true])
    addDirichletBC(s, [lx,0],[lx,0],[false,true])
    addNeumannBC(s, [lx/2, ly],[lx/2, ly],[0,1])
    setUP(s)
    solve(s)
    return s
end

function getSimpleBeamWithVoid(h=0.01)::Structure

    s = Structure(2,0.4,h)
    addDirichletBC(s, [0, 0], [0, 0], [true, true])
    addDirichletBC(s, [2, 0], [2, 0], [false, true])
    addNeumannBC(s, [0.7, 0.4], [1.3, 0.4], [0, 1])
    v1 = RectangularVoid([0.8, 0.15], [1.2, 0.25])
    v2 = CircularVoid([0.45,0.2], 0.1)
    v3 = CircularVoid([1.55,0.2], 0.1)
    addVoid(s, v1)
    addVoid(s, v2)
    addVoid(s, v3)
    setUP(s)
    solve(s)
    return s
end

function getBendingProblem(h=0.015)
    # h: Mesh-Size

    lx = 3
    ly = 3
    s = Structure(lx,ly,h)
    addDirichletBC(s, [lx-1, 0],[lx,0],[true,true])
    addNeumannBC(s, [0 , 2.01], [0 ,2.01], [0, 1])
    v = CircularVoid([0,0], 2)
    addVoid(s, v)
    setUP(s)
    solve(s)
    return s
end

function getDoublyClampedBeam(h=0.05)
    # h: Mesh-Size
    s = Structure(6,1,h);
    addDirichletBC(s, [0,0],[0,1],[true,true])
    addDirichletBC(s, [6,0],[6,1],[true,true])
    addNeumannBC(s, [3, 0.5],[3,0.5],[0,1])
    setUP(s)
    solve(s)
    return s
end
