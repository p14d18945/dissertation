include("TopOptProblem.jl")


abstract type OptimizationMethod end

mutable struct Optimizer
        ## FUNCTIONS FOR VISUALIZER
        ## add Typename and MOD to plot
        # plotInfo = true
        ## plotConvergenveTime
        # plotFTime = false
        ## save optimization as GIF
        # saveGIF = false

        # show optimization update
        printUpdate :: Bool

        # plotConvergenveTime
        plotFTime :: Bool

        # number of iterations
        nIt :: Int64

        # compliance
        cIt :: Vector{Float64}

        # convergenceTime
        conTime :: Vector{Float64}

        # StopCriteria for Optimization
        stopCrit :: Float64

        # optimization history
        history :: Array{Array{Float64,2},1}

        # topOptProblem Object
        top::TopOptProblem

        # optimization Routine i.e. OCRoutine
        or :: om where om <: OptimizationMethod

        function Optimizer()
                printUpdate = false
                plotFTime = false
                nIt = 0
                cIt = Vector{Float64}()
                conTime = Vector{Float64}()
                stopCrit = 1e-4
                history = Array{Array{Float64,2},1}()

                return new(printUpdate, plotFTime, nIt, cIt, conTime, stopCrit, history)
        end
end

function setOptimizationProblem(om::Optimizer, top::TopOptProblem)
        om.top = top
        return Nothing
end

function setOptimizationRoutine(om::Optimizer, or::OptRoutine) where OptRoutine <: OptimizationMethod
        om.or = or
        return Nothing
end

function getMOD(om::Optimizer)
        # measure of discreteness (MOD)
        xTry = getDensityMesh(om.top.s)
        vo = xtry
        vo .= om.top.volfrac
        omega = sum(vo)
        vTry = sum(sum(xTry.*(-xTry.+1)))
        mod = (1/omega*vTry)
        return mod
end

function setStopCriteria(om::Optimizer, stopCrit::Float64)
        # StopCriteria for Optimization
        om.stopCrit = stopCrit
end

function applyVoidElements(voidIDs::Vector{Bool}, x::Array{Float64,2})::Array{Float64,2}
        # find void Elements
        voidIDs = findall(voidIDs)
        # transpose density field because julia
        # fills matrix collumnwise
        xVoid = x'
        # set void elements to min density
        xVoid[voidIDs] .= 1e-5
        # set current density field
        xNew = xVoid'
        return xNew
  end

  function setFilterList(or :: optimizerRoutine, rho)::Array{Float64,2} where optimizerRoutine <: Optimizer
      # RhoFilter
      if !isempty(this.rhoFilterList)
          rfl = this.rhoFilterList;
          for k = 1:length(rfl)
              # currentFilter
              xPhys = rfl{k}.filterDensities(xPhys);
          end
      end
      # save filtered parameter
      rho = xPhys;
  end


  # plotF in VISUALIZER
  # makeGIF in VISUALIZER
