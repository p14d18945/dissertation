include("RhoFilter.jl")

# SIMP : Solid Isotropic Material with Penalization
struct SIMP <: RhoFilter
    # p : penaltyfactor i.e: p = 3
    p::Float64

    function SIMP(p::Float64)
        return new(p)
    end

    function SIMP(p::Int64)
        return new(Float64(p))
    end
end

function filterDensities(simp::SIMP, x::Array{Float64,2})::Array{Float64,2}
    # calculate rho
    rho = x.^simp.p
    return rho
end

function filterSensitivities(simp::SIMP, x::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    # calculate rhoPrime and gradDC
    rhoPrime = simp.p.* x.^(simp.p-1)
    # gradient
    dc = dc.*rhoPrime
    return dc
end
