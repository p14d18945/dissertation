include("RhoFilter.jl")

# RAMP : Rational Approximation of Material Properties
struct RAMP <: RhoFilter
    # q : rationalfactor i.e q = 5
    q::Float64

    function RAMP(p::Float64)
        return new(p)
    end

    function RAMP(p::Int64)
        return new(Float64(p))
    end
end

function filterDensities(ramp::RAMP, x::Array{Float64,2})::Array{Float64,2}
    # calculate rho
    rho = x./(ramp.q.*(-x.+1).+1)
    return rho
end

function filterSensitivities(ramp::RAMP, x::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    # calculate rhoPrime and gradDC
    rhoPrime = (ramp.q+1)./((x*ramp.q.-ramp.q.-1).^2)
    # gradient
    dc = dc.*rhoPrime
    return dc
end
