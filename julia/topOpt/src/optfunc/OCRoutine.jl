# oc routine fertigstellen
include("OptimizationMethod.jl")
include("UpdateType.jl")
include("StandardOCType.jl")
include("TTOType.jl")

using Printf
using Statistics


mutable struct OCRoutine <: OptimizationMethod
    # OCType-Object
    stepsize :: UpdateType
    # maximal amount of iterations
    itMax :: Int64
    # heaviside vol
    volumeFix :: Float64
    # object to increase beta
    useHeavisideFilter :: Bool
    heavisideObj :: HeavisideFilter



    function OCRoutine(stepsize::UpdateType)
        itMax = 1000
        volumeFix = -1.0
        useHeavisideFilter = false
        return new(stepsize, itMax, volumeFix, useHeavisideFilter)
    end
end

function applyDensityRegularization(densityFilterList::Vector{Filter}, xNew::Array{Float64,2})::Array{Float64,2}
    # Regularization/ RhoSensitiviy
    if !isempty(densityFilterList)
        for k = 1:length(densityFilterList)
            # currentFilter
            xNew = filterDensities(densityFilterList[k], xNew)
        end
    end
    # save filtered paramter
    xPhys = xNew
    return xPhys
end

function applySensitivityRegularization(oc::OCRoutine, sensitivityFilterList::Vector{Filter}, densityFilterList::Vector{Filter}, xNew, dc, vol)
    # Regularization

    # sensitivityFilter
    if !isempty(sensitivityFilterList)
        for k = 1:length(sensitivityFilterList)
            # currentFilter
            dc = filterSensitivities(sensitivityFilterList[k], xNew, dc)
        end
    end

    # DensityFilter
    if !isempty(densityFilterList)
        dfl = densityFilterList
        for k = 1:length(dfl)
            # currentFilter
            dc = filterSensitivities(dfl[k], xNew, dc);
            vol = filterVolumes(dfl[k], xNew, vol);
            # update increment (only for HeavisideF)
            if typeof(dfl[k]) == HeavisideFilter
                oc.heavisideObj = dfl[k]
            end
        end
    end
    # save filtered paramter
    dcF = dc
    dvF = vol

    return (dcF, dvF)
end

function applyRhoFilter(rhoFilterList::Vector{Filter}, xPhys::Array{Float64,2})::Array{Float64,2}
    # Regularization/ RhoSensitiviy
    # RhoFilter
    if !isempty(rhoFilterList)
        for k = 1:length(rhoFilterList)
            # currentFilter
            xPhys = filterDensities(rhoFilterList[k], xPhys)
        end
    end
    # save filtered parameter
    rho = xPhys
    return rho
end

function applyGradRhoFilter(rhoFilterList::Vector{Filter}, xPhys::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    # RhoSensitiviy
    if !isempty(rhoFilterList)
        for k = 1:length(rhoFilterList)
            # currentFilter
            dc = filterSensitivities(rhoFilterList[k], xPhys, dc);
        end
    end
    return dc
end

function updateX(oc :: OCRoutine, densityFilterList::Vector{Filter}, xVoids::Vector{Bool}, v0::Float64, l1::Float64, l2::Float64)
    # evolution object
    evo = oc.stepsize
    # Start
    vStep = 1.0

    cntBisec = 0

    while  (abs(vStep-v0)>10^-7 || (l2-l1)/(l1+l2) > 1e-3 )

        cntBisec += cntBisec
        # get new Step
        lmid = 0.5*(l2+l1)

        xNew = getXtrial(evo, lmid)
        # applyVoidElements
        global xNew = applyVoidElements(xVoids, xNew)

        # apply regularization (oleSigmund/ Heaviside...)
        global xPhys = applyDensityRegularization(densityFilterList, xNew)

        # cu#ent volume
        vStep = sum(xPhys)

        # updateLambda
        if vStep > v0
            l1 = lmid
        else
            l2 = lmid
        end

        # in case the heaviside filter is choosen
        # it's not possible to fullfill the volume constraint
        # volume fix:
        if oc.volumeFix > 0.0
            vStep = oc.volumeFix
        end
    end
    return (xNew, xPhys)
end




function solve(oc :: OCRoutine, o::Optimizer)

    ######### PREPARE ##########

    # topOptProblem
    tp = o.top

    # structure Object
    s = tp.s
    # voidList
    xVoids = s.voidIDs
    # volumefraction Mesh
    xNew = getDensityMesh(s)*tp.volfrac
    # start mesh for GIF
    # this.optMesh{1} = xNew;

    # objective volume
    v0 = sum(xNew)
    # volume Mesh
    vol = ones(size(xNew))

    # evolution method
    evo = oc.stepsize
    # setBoxConstraints
    setBoxConstraints(evo, tp.xL, tp.xU)

    # filterList
    if !isempty(tp.densityFilterList)
        setCoeffMatrix(tp.densityFilterList[1], xNew)
    elseif !isempty(tp.sensitivityFilterList)
        setCoeffMatrix(tp.sensitivityFilterList[1], xNew)
    end

    dfl = tp.densityFilterList
    sfl = tp.sensitivityFilterList
    rfl = tp.rhoFilterList

    # apply regularization (oleSigmund/ Heaviside...)
    xPhys = applyDensityRegularization(dfl, xNew)

    # init history
    c = 0.0
    cnt = 0
    elapsedTime = 0.0
    change = 1.0


    # first iteration does not count
    firstRun = true

    ########## start optimization ##############
    while change > o.stopCrit && cnt <= oc.itMax
        tic = @elapsed begin
            cnt += 1

            # save last compliance
            cOld = c

            # apply rhoFilter (SIMP/RAMP)
            rho = applyRhoFilter(rfl, xPhys)

            # update structure and solve
            setRhoMesh(s, rho)
            (c, dc) = solve(s)

            # apply gradRhoFilter (SIMP/RAMP)
            dc = applyGradRhoFilter(rfl, xPhys, dc)

            # apply sensitivity regularization (oleSigmund/ Heaviside...)
            (dc, dv) = applySensitivityRegularization(oc, sfl, dfl, xNew, dc, vol)

            # check if it's StandardOCStepSize

            if typeof(evo) == StandardOCType
                setVolume(evo, dv)
            end


            # check if it's TDO
            if typeof(evo) == TTOType
                # insert ElementSize
                h = getElementSize(s.m)
                setElementSizeVec(evo, h)
                # insert Laplacian
                lapl = getDensityLaplacian(s)
                setLaplacian(evo, lapl)
            end

            # Update Stepsize-Type Properties in evo-obj
            setDensity(evo, xNew)
            setGradient(evo, dc)

            # get specific lambdaBoundaries
            (l1, l2) = getLambdaBoundaries(evo)

            # save old x
            x = xNew

            # update x
            (xNew, xPhys) = updateX(oc, dfl, xVoids, v0, l1, l2)

            if !firstRun
                # update structure for plot and laplacian
                setDensityMesh(s, xPhys)

                # stopCriteria
                # change = (abs(c-cold)/c);
                # change = max(abs(xNew(:)-x(:)));

                # check if heavisidefilter is used
                # increase beta and update paramter
                if oc.useHeavisideFilter
                    oc.stopCrit = 0.01;
                    change = maximum(abs.(xNew-x))
                    f = oc.heavisideObj
                    change = updateParameter(f, change, oc.stopCrit)
                    oc.volumeFix = v0
                else
                    change = (abs(c-cOld)/c)
                end

                if o.printUpdate
                    status = @sprintf "It.:%4i   Obj.:%5.3f   Vol.:%5.3f   ch.:%5.4f\n" cnt c mean(xPhys) change
                    println(status)
                end

            end
                #if this.saveGIF
                #this.optMesh{cnt+1} = xPhys;
        end

        if !firstRun
            timeIncrement = tic
            push!(o.conTime, elapsedTime)
            elapsedTime = elapsedTime + timeIncrement
            push!(o.cIt, c)
        end

        if firstRun
            xNew = x
            xPhys = applyDensityRegularization(dfl, x)
            c = 0.0
            cnt = 0
            firstRun = false
        end
    end
    o.nIt = cnt;
end
