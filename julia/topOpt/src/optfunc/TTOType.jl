include("UpdateType.jl")

mutable struct TTOType <: UpdateType
    # stepsize boundary for iteration
    # betaStar, etaStar : numerical factors
    # i.e. : etaStar = 12, BetaStar = 2
    #        etaStar = 16, BetaStar = 8

    betaStar :: Float64
    etaStar :: Float64
    # volume
    vol :: Array{Float64,2}
    # density field
    x :: Array{Float64,2}
    # gradient of compliance
    gradC :: Array{Float64,2}
    # Box Constraints for optimization
    # xL : lower oj-func boundary
    xL :: Float64
    # xU : upper oj-func boundary
    xU :: Float64
    # elementsize vector
    h :: Float64
    # laplacian of rho
    lapl :: Array{Float64,2}
    # inner constructors

    function TTOType(betaStar :: n , etaStar :: n) where n <: Number
        return new(Float64(betaStar), Float64(etaStar))
    end

    function TTOType(betaStar :: n) where n <: Number
        etaStar = 12.0
        return new(Float64(betaStar), etaStar)
    end

end

function setLaplacian(tto :: TTOType , lapl :: Array{Float64,2})
    tto.lapl = lapl
    return Nothing
end

function setElementSizeVec(tto :: TTOType, h::Vector{<:Number})
    # insert elementsize h = [a , b] for increment
    tto.h = h[1] * h[2]
    return Nothing
end

function getBeta(tto :: TTOType, pw::Float64) :: Float64
    # calculate beta (regularization parameter)
    # pw = weighted average...
    #        ...gradient of compliance (driving force)

    beta = tto.betaStar*tto.h*pw
    return beta
end

function getEta(tto :: TTOType, pw::Float64) :: Float64
    # calculate eta (viscosity)
    # pw = weighted average...
    #        ...gradient of compliance (driving force)

    eta = tto.etaStar*pw
    return eta
end

function getWeightFunction(tto :: TTOType, x::Array{Float64}, dc::Array{Float64}) :: Float64
    # this function is useful to scale
    # viscosity (eta) and the regularization parameter (beta)
    # by the function g which describes amount of interdemiate ("gray") densities
    # xL, xU    : BoxConstraints (lower, upper)
    # x         : current step
    # dc        : Gradient of compliance (driving Force)
    # g         :
    g = (x.-tto.xL).*(-x.+tto.xU)
    pw = (sum(sum(g.*dc))./sum(sum(g)))
    return pw
end

function  getLambdaBoundaries(tto :: TTOType)
    # get lagrangian multiplier boundaries
    pw = getWeightFunction(tto, tto.x, tto.gradC)
    beta = getBeta(tto, pw)
    eta = getEta(tto, pw)

    lambdaLower = minimum((tto.gradC.+ beta.* tto.lapl).- eta)
    lambdaUpper = maximum((tto.gradC.+ beta.* tto.lapl).+ eta)
    return (lambdaLower, lambdaUpper)
end



function getTimeIncrement(tto :: TTOType) :: Float64
    # get Time increment for update scheme
    # n : size for number of inner loops
    n = (6/tto.etaStar)*(tto.betaStar)

    # deltaT : time increment 1/n
    deltaT = 1/n
    return deltaT
end

function getXtrial(tto :: TTOType, lambda :: Float64) :: Array{Float64,2}
    # x         : current step
    # dc        : Gradient of compliance (driving Force)
    # lambda    : scalar to scale (Lagrangian multiplier)
    # xTrial    : next Step
    dc = tto.gradC
    pw = getWeightFunction(tto, tto.x, dc)
    beta = getBeta(tto, pw)
    eta = getEta(tto, pw)
    deltaT = getTimeIncrement(tto)


    # lambdaLower = min(min((dc + beta * this.lapl))) - eta
    # lambdaUpper = max(max((dc + beta * this.lapl))) + eta

    # calculate next Step
    xTrial = deltaT/(eta).*(dc.-lambda.+beta.*tto.lapl) + tto.x


    # check box constraints
    xTrial = checkBoxConstraints(tto, xTrial)
    return xTrial
end
