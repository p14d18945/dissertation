include("Filter.jl")
using LinearAlgebra
using SparseArrays
# REGULARIZATIONFILTER to manipulate the sensitivities, densities i.e.
# Ole Sigmund, Heaviside...

# main type RegularizationFilter as subtype of Filter
abstract type RegularizationFilter <: Filter end
# subtype of Filter
abstract type SensitivityFilter <: RegularizationFilter end
# subtype of Filter
abstract type DensityFilter <: RegularizationFilter  end

# function for subtypes
function setCoeffMatrix(f::subfilter, x::Array{Float64,2}) where subfilter <: RegularizationFilter
        # number of elements in both directions
        (nely, nelx) = size(x)
        rmin = ceil(f.rmin)-1
        radSize = (2*rmin+1)^2

        iH = ones(Int64(nelx*nely*radSize))
        jH = ones(size(iH))
        hf = zeros(size(iH))
        cnt = 0

        for i1 = 1:nelx
            for j1 = 1:nely
                # elementNumber
                elemNum = (i1-1)*nely+j1

                # Boundaries for filtering
                leftB = max(i1 - rmin, 1)
                rightB = min(i1 + rmin, nelx)
                upperB = max(j1 - rmin, 1)
                lowerB = min(j1 + rmin, nely)

                for i2 = leftB:rightB
                    for j2 = upperB:lowerB
                        cnt = cnt+1

                        # number of element inside radius
                        elemNumF = (i2-1)*nely+j2

                        iH[cnt] = elemNum
                        jH[cnt] = elemNumF

                        # Euclidian norm between centroids
                        # of neighbour elements
                        # w = sqrt((i1-i2)^2+(j1-j2)^2)
                        distX = i1-i2
                        distY = j1-j2
                        w = getWeightFunction(f, distX, distY)

                        # choose elements inside radius
                        hf[cnt] = max(0, w)
                    end
                end
            end
        end
        f.H = sparse(iH, jH, hf)
        f.Hs = sum(f.H, dims=2)
        return (f.H, f.Hs)
end
