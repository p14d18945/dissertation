include("UpdateType.jl")

mutable struct StandardOCType <: UpdateType
    # stepsize boundary for iteration
    # i.e. move = 0.2
    move :: Float64
    # volume
    vol :: Array{Float64,2}
    # density field
    x :: Array{Float64,2}
    # gradient of compliance
    gradC :: Array{Float64,2}
    # Box Constraints for optimization
    # xL : lower oj-func boundary
    xL :: Float64
    # xU : upper oj-func boundary
    xU :: Float64

    # inner constructor
    StandardOCType(move) = new(move)
end

function setVolume(sOCT :: StandardOCType , vol :: Array{Float64,2})
    sOCT.vol = vol
    return Nothing
end

function getXtrial(sOCT :: StandardOCType , lambda::Float64)
    # x         : current step
    # dc        : Gradient of compliance (driving Force)
    # lambda    : scalar to scale (Lagrangian multiplier)
    # xTrial    : next Step
    # steplimits
    maxStep = sOCT.x.+sOCT.move
    minStep = sOCT.x.-sOCT.move
    # current steptrial
    xTrial = sOCT.x.*sqrt.(sOCT.gradC./sOCT.vol./lambda)

    # move limit
    xTrial = max.(minStep, min.(maxStep, xTrial));

    # check box constraints
    xTrial = checkBoxConstraints(sOCT, xTrial)
end

function getLambdaBoundaries(sOCT :: StandardOCType)
    # get lagrangian multiplier boundaries
    lambdaLower = 0.0
    lambdaUpper = 1e9
    return (lambdaLower, lambdaUpper)
end
