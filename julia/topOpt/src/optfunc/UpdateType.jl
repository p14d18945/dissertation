abstract type UpdateType end


function setBoxConstraints(sU::subUpdate, xL::Float64, xU::Float64) where subUpdate <: UpdateType
    # Box Constraints for optimization
    # xL : lower oj-func boundary
    # xU : upper oj-func boundary
    sU.xL = xL
    sU.xU = xU
    return Nothing
end

function setDensity(sU::subUpdate, x::Array{Float64,2}) where subUpdate <: UpdateType
    # insert density field
    sU.x = x
    return Nothing
end

function setGradient(sU::subUpdate, dc::Array{Float64,2}) where subUpdate <: UpdateType
    # insert gradient of rho
    sU.gradC = dc
end

function checkBoxConstraints(sU::subUpdate, x::Array{Float64,2}) where subUpdate <: UpdateType
    xTrial = max.(sU.xL, min.(sU.xU, x))
    return xTrial
end
