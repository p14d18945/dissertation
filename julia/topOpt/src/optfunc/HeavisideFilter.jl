include("RegularizationFilter.jl")

using Printf

mutable struct HeavisideFilter <: DensityFilter
    # Filter radius i.e. 0.03*nelx
    rmin :: Float64
    # beta increment
    beta :: Int64
    # beta counter
    loopbeta :: Int64
    # coeff matrix
    H :: SparseMatrixCSC{Float64, Int64}
    Hs :: Array{Float64,2}
    function HeavisideFilter(rmin)
        # beta increment
        beta = 1
        # beta counter
        loopbeta = -3
        return new(rmin, beta, loopbeta)
    end
end

function getXTilde(hf :: HeavisideFilter, xNew::Array{Float64,2})::Array{Float64,2}
    # Calculate xTilde
    xTilde = (hf.H * xNew[:])./hf.Hs
    return xTilde
end

function getDX(hf :: HeavisideFilter, xTilde::Array{Float64,2})::Array{Float64,2}
    dx = hf.beta.*exp.(-hf.beta*xTilde).+exp.(-hf.beta)
    #dx = 1-exp(-this.beta*xTilde)+xTilde*exp(-this.beta);
    return dx
end

function filterDensities(hf::HeavisideFilter , xNew::Array{Float64,2})::Array{Float64,2}
    # first iteration
    if hf.loopbeta == -3
        # xTilde is equal to xNew
        xTilde = xNew
        # calc. xPhys
        xPhys = -exp.(-hf.beta*xTilde).+ 1 + xTilde.* exp.(-hf.beta)

        # set loop beta to -2
        hf.loopbeta += hf.loopbeta
    else
        # remaining iterations
        xTilde = getXTilde(hf, xNew)
        # filter densities
        xPhys = -exp.(-hf.beta*xTilde).+ 1 + xTilde.* exp.(-hf.beta)
        # sparse to Matrix
        (nely, nelx) = size(xNew)
        xPhys = reshape(xPhys, nely, nelx)
    end
    return xPhys
end

function filterSensitivities(hf::HeavisideFilter , xNew::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    if hf.loopbeta == -2
        # xTilde is equal to xNew
        xTilde = xNew
        # set loop beta to -1
        hf.loopbeta += hf.loopbeta
    else
        # remaining iterations
        xTilde = getXTilde(hf, xNew)
    end
    # get dX
    dx = getDX(hf, xTilde)

    # filter sensitivites
    dc = hf.H *(dc[:].*dx[:]./hf.Hs)
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    dc = reshape(dc, nely, nelx)
    return dc
end

function filterVolumes(hf::HeavisideFilter , xNew::Array{Float64,2}, vol::Array{Float64,2})::Array{Float64,2}
    if hf.loopbeta == -1
        # xTilde is equal to xNew
        xTilde = xNew
        # set loop beta to 0
        hf.loopbeta += hf.loopbeta
    else
        # remaining iterations
        xTilde = getXTilde(hf, xNew)
    end
    # get dX
    dx = getDX(hf, xTilde)
    # filter volumes
    dv = hf.H *(vol[:].*dx[:]./hf.Hs)
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    dv = reshape(dv, nely, nelx)
    return dv
end

function getWeightFunction(hf::HeavisideFilter, distX::Float64, distY::Float64)
    norm = sqrt(distX^2+distY^2)
    w = hf.rmin - norm
    return w
end

function updateParameter(hf::HeavisideFilter, change::Float64, stopCrit::Float64)::Float64
    # update counter
    hf.loopbeta += hf.loopbeta

    # update beta
    if hf.beta < 512 && (hf.loopbeta >= 50 || change <= stopCrit)
        hf.beta = 2 * hf.beta
        hf.loopbeta = 0
        changeUpdate = 1
        b = hf.beta
        s = @sprintf "Parameter beta increased to %1.i" b
        println(s)
    else
        # keep change, if beta is not increased
        changeUpdate = change
    end
    return changeUpdate
end
