include("RegularizationFilter.jl")

mutable struct OleSigmundFilter <: SensitivityFilter
    # Filter radius i.e. 0.042*nelx
    rmin :: Float64
    # coeff matrix
    H :: SparseMatrixCSC{Float64, Int64}
    Hs :: Array{Float64,2}
    OleSigmundFilter(rmin) = new(rmin)
end

function filterSensitivities(bf::OleSigmundFilter , xNew::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    # filter sensitivites
    dc = bf.H *(dc[:]./bf.Hs)

    dc = bf.H *(xNew[:].*dc[:])./bf.Hs./max.(1e-4,xNew[:])
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    dc = reshape(dc, nely, nelx)
    return dc
end

function getWeightFunction(bf::OleSigmundFilter, distX::Float64, distY::Float64)
    norm = sqrt(distX^2+distY^2)
    w = bf.rmin - norm
    return w
end
