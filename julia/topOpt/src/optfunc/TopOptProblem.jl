# Structure
include("../fefunc/Structure.jl")

include("Filter.jl")
# RegularizationFilter <: Filter
include("RegularizationFilter.jl")
# SensitivityFilter <: RegularizationFilter
include("OleSigmundFilter.jl")
# DensityFilter <: RegularizationFilter
include("BrunsTortorelliFilter.jl")
include("HeavisideFilter.jl")
# RhoFilter <: Filter
include("RhoFilter.jl")
include("SIMPFilter.jl")
include("RAMPFilter.jl")


mutable struct TopOptProblem
    # Structure object
    s :: Structure
    # volume fraction
    volfrac :: Float64

    # Box Constraints
    # xL : lower oj-func boundary
    xL :: Float64
    # xU : upper oj-func boundary
    xU :: Float64
    # filterList : SIMP, oleSigmund, heaviside...
    unsrtFilter :: Vector{Filter}
    densityFilterList :: Vector{Filter}
    sensitivityFilterList :: Vector{Filter}
    rhoFilterList :: Vector{Filter}

    # inner constructor
    function TopOptProblem(s::Structure)
        volfrac = 0.5
        xL = 1e-4
        xU = 1.0
        unsrtFilter = Vector{Filter}()
        densityFilterList = Vector{Filter}()
        sensitivityFilterList = Vector{Filter}()
        rhoFilterList = Vector{Filter}()

        return new(s, volfrac, xL, xU, unsrtFilter, densityFilterList, sensitivityFilterList, rhoFilterList)
    end

end

function setVolfrac(top::TopOptProblem, vol::Float64)
    # set the volume fraction
    # useful to set the objective Vol.
    top.volfrac = vol
    return Nothing
end


function setBoxConstraints(top::TopOptProblem, xL::Float64, xU::Float64)
    # Box Constraints for optimization
    # xL : lower oj-func boundary
    # xU : upper oj-func boundary
    top.xL = xL
    top.xU = xU
    return Nothing
end

function addFilter(top::TopOptProblem, f::Filter)
    # Sensitivity filter to remove chequerboarding
    # filter : Filter Object
    push!(top.unsrtFilter, f)
    sortFilterList(top)
    return Nothing
end

function sortFilterList(top::TopOptProblem)
    if !isempty(top.unsrtFilter)
        # get all used filterTypes
        f_types = typeof.(top.unsrtFilter)

        # get RhoFilter IDs
        rhoIDs = findall(f_types -> f_types<:RhoFilter, f_types)
        # get DensityFilter IDs
        densIDs = findall(f_types -> f_types<:DensityFilter, f_types)
        # get SensitivityFilter IDs
        sensIDs = findall(f_types -> f_types<:SensitivityFilter, f_types)
        # get amount of used Filters
        top.rhoFilterList = push!(top.unsrtFilter[rhoIDs])
        top.densityFilterList = push!(top.unsrtFilter[densIDs])
        top.sensitivityFilterList = push!(top.unsrtFilter[sensIDs])
    end
    return Nothing
end
