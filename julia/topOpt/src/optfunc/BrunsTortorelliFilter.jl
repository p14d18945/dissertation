include("RegularizationFilter.jl")

mutable struct BrunsTortorelliFilter <: DensityFilter
    # Filter radius i.e. 0.042*nelx
    rmin :: Float64
    # coeff matrix
    H :: SparseMatrixCSC{Float64, Int64}
    Hs :: Array{Float64,2}
    BrunsTortorelliFilter(rmin) = new(rmin)
end

function filterDensities(bf::BrunsTortorelliFilter , xNew::Array{Float64,2})::Array{Float64,2}
    # filter densities
    xPhys = (bf.H *xNew[:])./bf.Hs
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    xPhys = reshape(xPhys, nely, nelx)
    return xPhys
end

function filterSensitivities(bf::BrunsTortorelliFilter , xNew::Array{Float64,2}, dc::Array{Float64,2})::Array{Float64,2}
    # filter sensitivites
    dc = bf.H *(dc[:]./bf.Hs)
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    dc = reshape(dc, nely, nelx)
    return dc
end

function filterVolumes(bf::BrunsTortorelliFilter , xNew::Array{Float64,2}, vol::Array{Float64,2})::Array{Float64,2}
    # filter volumes
    dv = bf.H *(vol[:]./bf.Hs)
    # sparse to Matrix
    (nely, nelx) = size(xNew)
    dv = reshape(dv, nely, nelx)
    return dv
end

function getWeightFunction(bf::BrunsTortorelliFilter, distX::Float64, distY::Float64)
    norm = sqrt(distX^2+distY^2)
    w = exp((-1/2)*(norm/(bf.rmin/3))^2)
    return w
end
