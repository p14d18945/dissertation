include("Filter.jl")
# RhoFunction is useful to calcute the
# density by a given designVariable
# rho(x)

# main type RhoFilter as subtype of Filter
abstract type RhoFilter <: Filter end
