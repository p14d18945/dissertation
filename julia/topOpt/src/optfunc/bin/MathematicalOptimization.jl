
include("OptimizationMethod.jl")


mutable struct MathematicalOptimization <: Optimizer
    # Supertype of i.e. OCRoutine
    # seperated Filterlists to
    # increase optimization performance
    densityFilterList :: Vector{Filter}
    sensitivityFilterList :: Vector{Filter}
    rhoFilterList :: Vector{Filter}

    # constructor
    function MathematicalOptimization()
        densityFilterList = Vector{Filter}()
        sensitivityFilterList = Vector{Filter}()
        rhoFilterList = Vector{Filter}()

        return new(densityFilterList, sensitivityFilterList, rhoFilterList)
    end
end

function seperateFilter(mo :: MathematicalOptimization, om :: OptimizationMethod)
    # topOptProblem
    tp = om.top
    # init counter
    cntRF = 0
    cntDF = 0
    cntSF = 0
    if ~isempty(tp.filter)
        # get all used filterTypes
        f_types = typeof.(tp.filter)

        # get RhoFilter IDs
        rhoIDs = findall(f_types -> f_types<:RhoFilter, f_types)
        # get DensityFilter IDs
        densIDs = findall(f_types -> f_types<:DensityFilter, f_types)
        # get SensitivityFilter IDs
        sensIDs = findall(f_types -> f_types<:SensitivityFilter, f_types)
        # get amount of used Filters
        nRhoF = length(rhoIDs)
        nDensF = length(densIDs)
        nSensF = length(sensIDs)

        mo.rhoFilterList = push!(tp.filter[rhoIDs])
        mo.densityFilterList = push!(tp.filter[densIDs])
        mo.sensitivityFilterList = push!(tp.filter[sensIDs])
    end
    return Nothing
end
