include("src/optfunc/OCRoutine.jl")
include("src/postprocess/Visualizer.jl")
include("src/factory/Factory.jl")

# h = 0.05
#     s = getMBBeam(h)
#     v = Visualizer(s)
#     plot(v);
#
# simp = SIMP(3)
#     topOpt = TopOptProblem(s)
#     #ol = OleSigmundFilter(0.126/h)
#     setBoxConstraints(topOpt, 1e-5, 1.0)
#
#     addFilter(topOpt, simp)
#     #addFilter(topOpt, ol)
#
#     om = Optimizer()
#     om.printUpdate = false
#     setOptimizationProblem(om, topOpt)
#
#     #soc = StandardOCType(0.2)
#     soc = TTOType(3.0)
#     oc = OCRoutine(soc)
#
#     @time begin
#         solve(oc, om)
#     end
#
# v = Visualizer(s)
#     om.plotFTime = true
#     scene = plot(v)


h = 0.02
    s = getDoublyClampedBeam(h)
    s.x = s.x
    v = Visualizer(s)
    plot(v);

simp = SIMP(3)
    topOpt = TopOptProblem(s)
    #ol = OleSigmundFilter(0.126/h)
    setBoxConstraints(topOpt, 1e-5, 1.0)

    addFilter(topOpt, simp)
    #addFilter(topOpt, ol)

    om = Optimizer()
    om.printUpdate = false
    setOptimizationProblem(om, topOpt)

    #soc = StandardOCType(0.2)
    soc = TTOType(2.0)
    oc = OCRoutine(soc)

    @time begin
        solve(oc, om)
    end
include("src/postprocess/Visualizer.jl")
v = Visualizer(om)
    om.plotFTime = true
    v.showMesh = false
    scene = plotf(v)
plotf(v)
