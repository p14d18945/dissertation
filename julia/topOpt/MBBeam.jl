include("src/optfunc/OptimizationMethod.jl")
include("src/optfunc/TTOType.jl")
include("src/optfunc/OCRoutine.jl")
h = 0.002
s = Structure(3,1,h)
addDirichletBC(s, [3-h,1],[3,1],[false,true])
addDirichletBC(s, [0,0],[0,1],[true,false])
addNeumannBC(s,[0,0],[h,0],[0,1])
setUP(s)
solve(s)

simp = SIMP(3)

topOpt = TopOptProblem(s)
addFilter(topOpt, simp)

om = Optimizer()
om.printUpdate = true
setOptimizationProblem(om, topOpt)

tto = TTOType(2)
oc = OCRoutine(tto)

solve(oc, om)

sum(s.x)

using Plots;

heatmap(-s.x, c = :greys)
