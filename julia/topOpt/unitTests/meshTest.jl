include("../+src/fefunc/Mesh.jl")
using Test

@testset "meshTests" begin
    # create object
    m = Mesh(1.8, 1.5, 0.5)
    init(m)

    # UnitTest Elementsize
    eSize = m.eSize
    @test eSize == Array{Float16}([0.6, 0.5])

    # UnitTest countElements
    nE = countElements(m)
    @test nE == 9

    # UnitTest countNodes
    nN = countNodes(m)
    @test nN == 16

    # UnitTest getElementNodes (clockwise)
    n = getElementNodes(m, 9)
    @test n == Array{Int32}([11, 12, 16, 15])

    # UnitTest getNodePosition zero = upper leftcorner
    np = getNodePosition(m, 11)
    @test round.(np*100)/100 == Array{Float32}([1.20, 1.00])

    # UnitTest getElementCenter
    p = getElementCenter(m, 5)
    @test round.(p*100)/100 == Array{Float32}([0.9, 0.75])
end
