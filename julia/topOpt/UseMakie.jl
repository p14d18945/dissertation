# TO_DO
# using PKG
# Pkg.add["Makie"]
# Pkg.add["Colors"]
# Pkg.add["GeometryTypes"]
# Pkg.add["GLMakie"]
# Pkg.add.(["PackageCompiler",
#   "AbstractPlotting", "GDAL",
#   "GeometryTypes", "MakieGallery",
#   "RDatasets","CairoMakie",
#   "WGLMakie", "StatsMakie"])
using Makie
using AbstractPlotting
using Colors
using GeometryTypes
using GLMakie

coordinates = [
     0.0 0.0;
     0.5 0.0;
     1.0 0.0;
     0.0 0.5;
     0.5 0.5;
     1.0 0.5;
     0.0 1.0;
     0.5 1.0;
     1.0 1.0;
     ]

connectivity = [
     1 5 2;
     1 4 5;
     2 3 6;
     2 5 6;
     4 5 8;
     4 7 8;
     5 6 9;
     5 8 9;
     ]

cordinates = Array{Point,2}(undef, 4, 1)
cordinates[1] = (1.0, 0.0)
cordinates[2] = (1.0, -1.0)
cordinates[3] = (0.0, -1.0)
cordinates[4] = (1.0, -1.0)

connectivity = [
          1 4 3 2;
          ]

color = [0.0, 0.0, 0.0, 0.0, 1, 0.0, 0.0, 0.0, 0.0]
color = [0.3, 0.2, 0.5, 1.0]
poly(coordinates, connectivity, color = color, strokecolor = (:black, 0.6), strokewidth = 4)
scene = mesh(coordinates, connectivity, color = color, shading = false)
wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)



using Makie, GeometryTypes, AbstractPlotting
AbstractPlotting.set_theme!(
    plot = (show_axis = false, scale_plot = false),
    color = :turquoise1
)
poly(HyperRectangle(coordinates), strokecolor = (:black, 0.6), strokewidth = 4)
HyperRectangle(Vec2f0(0), Vec2f0(100))
AbstractPlotting.

image(-s.x')


###############

# Lets take SimpleRectangle as an example:
# Minimal set of decomposable attributes to build up a triangle mesh
isdecomposable(::Type{T}, ::Type{HR}) where {T<:Point, HR<:SimpleRectangle} = true
isdecomposable(::Type{T}, ::Type{HR}) where {T<:Face, HR<:SimpleRectangle} = true

# Example implementation of decompose for points
function GeometryTypes.decompose(P::Type{Point{3, PT}}, r::SimpleRectangle, resolution=(2,2)) where PT
    w,h = resolution
    vec(P[(x,y,0) for x=range(r.x, stop = r.x+r.w, length = w), y=range(r.y, stop = r.y+r.h, length = h)])
end

function GeometryTypes.decompose(::Type{T}, r::SimpleRectangle, resolution=(2,2)) where T <: Face
    w,h = resolution
    Idx = LinearIndices(resolution)
    faces = vec([Face{4, Int}(
            Idx[i, j], Idx[i+1, j],
            Idx[i+1, j+1], Idx[i, j+1]
        ) for i=1:(w-1), j=1:(h-1)]
    )
    decompose(T, faces)
end


rect = SimpleRectangle(0, 0, 1, 1)
m = GLNormalMesh(rect)
vertices(m) == decompose(Point3f0, rect)

faces(m) == decompose(GLTriangle, rect) # GLFace{3} == GLTriangle
normals(m) # automatically calculated from mesh

mesh(m)
wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)
