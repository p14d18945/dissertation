include("src/optfunc/OCRoutine.jl")
include("src/postprocess/Visualizer.jl")
include("src/factory/Factory.jl")
s = getBendingProblem()
s = getDoublyClampedBeam()
s = getMBBeam()
s.x = s.x*0.5

v = Visualizer(s)
plot(v)





#    h = 0.01
#    lx = 2
#    s = Structure(lx,0.5,h)
#    d = addDirichletBC(s, [2, 0], [2, 0.5], [true, true])
#    addNeumannBC(s, [lx, 0.0], [lx, 0.5], [1, 4])
#    addDirichletBC(s, [0, 0], [lx, 0], [true, true])
#    addNeumannBC(s, [0, 0.5], [lx, 0.5], [1, 1])
#    setUP(s)
#    s.x = s.x*0.5
#solve(s)

#include("src/postprocess/Visualizer.jl")
#    v = Visualizer(s)
#v.showMesh = false
    #v.drawLoads = true


#    scene = plot(v);
    #scene = drawTriangleZ(v, scene, 1, [true, true]);
    #scene = drawTriangleZ(v, scene, 2, [true, true]);
    #scene = drawTriangleZ(v, scene, 157, [true, true], false);
    #scene = drawTriangleZ(v, scene, 3, [true, false]);
    #scene = drawTriangleZ(v, scene, 157, [true, false], false);
    #scene = drawTriangleX(v, scene, 1, [true, true], true);
    #scene = drawTriangleX(v, scene, 27, [false, true])
    #scene = drawDirichlets(v, scene, d);
#    scene


lineStart = [pos[1]-scale*0.2, pos[2]-scale*0.3-(i-1)*linelength/4]
lineEnd = [pos[1]+scale*0.4, pos[2]-scale*0.5-(i-1)*linelength/4]
