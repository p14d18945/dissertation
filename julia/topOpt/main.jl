# Structure
include("src/optfunc/OptimizationMethod.jl")
include("src/optfunc/TTOType.jl")
include("src/optfunc/OCRoutine.jl")
#include("src/optfunc/MathematicalOptimization.jl")
s = Structure(1,1,0.2)
rv = RectangularVoid([1,1], [2.5,2])
addVoid(s, rv)
d = addDirichletBC(s::Structure, [0,0], [0,1], [true, true])
n = addNeumannBC(s::Structure, [1,0], [1,1], [1, 1])
setUP(s)
(c, gradC) = solve(s)

simp = SIMP(3)
ramp = RAMP(5)
ol = OleSigmundFilter(3)
h = HeavisideFilter(5)
topopt = TopOptProblem(s)
addFilter(topopt, ol)
addFilter(topopt, ramp)
addFilter(topopt, simp)

topopt.unsrtFilter

isempty(topopt.densityFilterList)
topopt.sensitivityFilterList[1]
topopt.rhoFilterList[]

om = Optimizer()

setOptimizationProblem(om, topopt)


tto = TTOType(8)

oc = OCRoutine(tto)

setOptimizationRoutine(om, oc)
solve(oc, om)#

om.printUpdate = true
include("src/optfunc/OCRoutine.jl")




(xNew, xPhys) = updateX(oc, dfl, xVoids, v0, l1, l2);
dfl = topopt.densityFilterList
xVoids = s.voidIDs
(l1,l2) = getLambdaBoundaries(tto)
xN = getDensityMesh(s)*topopt.volfrac
v0 = sum(xN)

evo = oc.stepsize
# Start
vStep = 1.0


cntBisec
cntBisec += 1


a = 1

function updateY(oc, densityFilterList, xVoids, v0, l1, l2)
    # evolution object
    # Start
    vStep = 1.0

    cntBisec = 0
    println(12)

    while  (abs(vStep-v0)>10^-7 || (l2-l1)/(l1+l2) > 1e-3) == true
        cntBisec += 1
        println(cntBisec)
        # get new Step
        lmid = 0.5*(l2+l1)

        xNew = getXtrial(oc.stepsize, lmid)
        # applyVoidElements
        global xNew = applyVoidElements(xVoids, xNew)

        # apply regularization (oleSigmund/ Heaviside...)
        global xPhys = applyDensityRegularization(densityFilterList, xNew)

        # cu#ent volume
        vStep = sum(xPhys)

        # updateLambda
        if vStep > v0
            l1 = lmid
        else
            l2 = lmid
        end

        # in case the heaviside filter is choosen
        # it's not possible to fullfill the volume constraint
        # volume fix:
        if oc.volumeFix > 0.0
            vStep = oc.volumeFix
        end
            #lmid = 0.5*(l2+l1);
    end
    return (xNew, xPhys)
end

updateY(oc, dfl, xVoids, v0, l1, l2)
xNew = getXtrial(oc.stepsize, 0.5*(l2+l1))

function loop(a)
    while a < 4
        a += 1
        println(a)
    end
    return a
end

f = loop(1)
