include("src/optfunc/OptimizationMethod.jl")

s = Structure(1.5, 1, 0.5)

addDirichletBC(s, [0,0],[0,1],[false,true])
addDirichletBC(s, [0,0],[0,1],[true,false])

addNeumannBC(s,[1.5,0],[1.5,1],[0,1])
setUP(s)
(c, gradC)=solve(s)
